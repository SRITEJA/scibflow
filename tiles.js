var recordsForScroll = 15;
var tileTypes = [ 'task', 'post', 'query' ];
var selectedToList = [];
var attachedIds = [];
var selectedTagsList=[];
var leaveappid=null;

var updateTileTemplate = {
	"tileid" : "",
	"attributesToBeChanged" : "",
	"attributeMapsToBeChanged" : "",
	"updatedby" : ""
};

var attributesTemplate = {
	"duedate" : ""
};

var attributeMapTemplate = {
	"assignees" : ""
};

var actionTemplate = {
	"response" : ""	
};
var progressTemplate={
		"progress":""
}

var workspacename = null;
/**
 * @param authorId
 * @param author
 * @param avatar
 * @returns {String}
 */
function showAvatar(authorId, author, avatar, bMinimized, imageForHeader) {
	var sb = "";
	var height= "30px";
 	var textwidth= "30px";
 	var imagewidth= "30px";
	var lineheight = "30px";
	var nameofClass="scrib-x4";
	var fontsize="18px"
	if(bMinimized){
		height= "20px";
 		textwidth= "30px";
 		imagewidth= "20px";
		lineheight = "20px";
		nameofClass="scrib-x4"
			fontsize="18px"
	}
    if(imageForHeader){
    	height= "40px";
 		textwidth= "40px";
 		imagewidth= "40px";
		lineheight = "40px";
		nameofClass="scrib-x6"
		fontsize="22px"
    }
	
	if (isValueNotEmpty(avatar)) {

		sb += "<img style='float:left' src='serv/afile?filepathtype=&name="
		+ avatar + "' width='"+imagewidth+"'  height='"+height+"'/>";
	}
	
	else {
		var colorClass = getAvatarReplacementColorClass(authorId)+" "+nameofClass;
		sb += "<div style='float:left;width:"+textwidth+";height:"+height+";  font-size :"+fontsize+" ;line-height:"+lineheight+";text-align:center;color:#FFF;font-family: RobotoCondensed,Arial,sans-serif;'"
   + "class= "	+ colorClass + ">";
		if (isValueNotEmpty(author)) {
			var temp = author.split(" ");
			for (i = 0; i < temp.length; i++) {
				if(i<=1){
					sb += temp[i].charAt(0).toUpperCase();
				}
			}
		} else {
			sb += authorId.charAt(0).toUpperCase();
		}
		sb += "</div>";
	}
	return sb;
}

Handlebars.registerHelper('showAvatar', function(authorid, author, avatar,bMinimized,imageForHeader) {
	return new Handlebars.SafeString(showAvatar(authorid, author, avatar,bMinimized,imageForHeader));
});

/**
 * to show user image
 * @param avatar
 * @returns {String}
 */
function showImage(avatar,author,authorId) {
	var sb = "";
	if (isValueNotEmpty(avatar)) {
		sb += "<img style='float:left'  class='media-object scrib-tileavatar' alt='User' src='serv/avatar/" + HOME_ID + "/"+ avatar + "' width='30px' height='30px'/>";
	} else {
		sb += "<img style='float:left'  class='media-object scrib-tileavatar' alt='User' src='assets/images/no_photo.png' width='30px' height='30px'/>";
	
	}
	return sb;
}

Handlebars.registerHelper('showImage', function(avatar,author,authorId) {
	return new Handlebars.SafeString(showImage(avatar,author));
});

Handlebars.registerHelper('showImageOnListView', function(avatar) {
	return new Handlebars.SafeString(showImageOnListView(avatar));
});



Handlebars.registerHelper('showUser', function(authorId) {
	
	return new Handlebars.SafeString(showUser(authorId));
});



/**
 * get user name from user object
 * @param user
 * @returns {String}
 */
function getUserName(user){
	if(!user) return "";
	return user.fname+" "+user.lname;
}
/**
 * to return avatar for list view
 * @param avatar
 * @returns {String}
 */

function showImageOnListView(avatar) {
	var sb = "";
	if (isValueNotEmpty(avatar)) {
		sb+="serv/afile?filepathtype=&name="
			+ avatar ;
	}
	return sb;
}





Handlebars.registerHelper('avatarReplacement', function(author,authorId) {
	
		return new Handlebars.SafeString(avatarReplacement(author,authorId));
});
/**
 * 
 * to return avatar replacement
 * @param author
 * @param authorId
 * @returns {String}
 */

function avatarReplacement(author,authorId){
	var sb="";
	var colorClass = getAvatarReplacementColorClass(authorId)+" "+"scrib-x6";
	sb += "<div style='text-align:center;color:#FFF; height:40px;width:40px ;font-size:22px;font-family: RobotoCondensed,Arial,sans-serif;' class='" + colorClass + 
	"media-object scrib-tileavatar'>";
	var temp = author.split(" ");
	sb +="<span style='line-height:40px;'>";
	for (i = 0; i < temp.length; i++) {
		sb += temp[i].charAt(0).toUpperCase();
	}
	sb += "</span></div>";
	return sb;
}



Handlebars.registerHelper('getSelectedActivityName', function(activityid) {
	if (activityid == HOME_ID)
		return " Public";
	else
		return new Handlebars.SafeString(" "+getSelectedActivityName(activityid));
});



Handlebars.registerHelper('getSelectedActivityNameForListView', function(activityid) {
	if (activityid == HOME_ID)
		return " Public";
	else
		return new Handlebars.SafeString(" "+getSelectedActivityNameForNotification(activityid));
});

Handlebars.registerHelper('getSelectedActivityNameForSearchView', function(activityid) {
	if (activityid == HOME_ID)
		return " All";
	else
		return new Handlebars.SafeString(" "+getSelectedActivityName(activityid));
});



/**
 * to show comment box
 * @param id
 */
function showCommentBox(id,isLightBox) {
	 makeCreateListEmpty();
	 $(".file-comments-status").find("span").remove();
	if(isLightBox){		
		$("#lightBox_comment_"+id).toggle();
		$("#text_detail_"+id).val("");
		if(!isIe){
		$("#text_detail_"+id).focus();
		}
		
	}
	else{
	$("#tile_comment_" +id).toggle();
	$("#text_" +id).val("");
	if(!isIe){
		$("#text_" +id).focus();
		}
	rearrangeTiles();
	showplc();
	}
	
		$("#text_" +id).keypress(function(){
			removePlaceholderClass("#text_" +id);
	});
	
}

/**
 * get Tile Object for a given ID from list of tiles
 * @param tileid
 * @returns
 */
function getTile(tileid){
 var returnObj = null;
	$.each(tileData,function(i,tile){
			if(tile.id == tileid){
				returnObj =  tile;
			}
		});
	return returnObj;
}


/**
 * to create comment
 * @param text
 * @param tileid
 * @param activityid
 * @param callBackMethod
 * @param callBackArguments
 */
function createComment(text, tileid, activityid, callBackMethod, callBackArguments){
	

	
	if(text!=null&&text!=""){
		$("#text_"+tileid).removeClass("red");
		
			removePlaceholderClass("#text_"+tileid);

		var jsondata = JSON.stringify([ {
			tileid : tileid,
			commenttext : text,
			attachment : attachedIds,
			createdby : getUserId(),
			activityid:activityid
		} ]);
	
		var postData = {
			"jsondata" : jsondata
		};
		performAJAX(getCreateCommentURL(), "POST", postData, "",
				callBackMethod, callBackArguments,
				"application/x-www-form-urlencoded");
	}else{
		$("#text_"+tileid).attr("placeholder","Please enter your reply.");
		$("#text_"+tileid).addClass("red");
			addPlaceholderClass("#text_"+tileid);
	showplc();
	}
}
/**
 * to add comment on tile
 * @param tileId
 */
function addCommentOnTile(tileId,tiletype) {
	clearValue();
	var text = $("#text_"+tileId).get(0).value;

	
	var callBackArguments = {
		"tileId" : tileId,
		"tiletype":tiletype
	};
	

	text = text.replace(/(\r\n|\n|\r)/gm, "<br>");
	

	var activityid = getTile(tileId).activityid;
	
	createComment(text,tileId,activityid,addComment_callBack,callBackArguments);  
}
/**
 * to add comment from lightbox
 * @param tileId
 */
function addCommentOnTileOnLB(tileId,typeOfTile) {
	clearValue();
	var text = $("#text_detail_"+tileId).get(0).value;
	
	$("#text_detail_"+tileId).val("");
	var callBackArguments = {
			"tileId" : tileId,
			"typeOfTile":typeOfTile			
	};
	

	text = text.replace(/(\r\n|\n|\r)/gm, "<br>");
	var activityid = getTileLB(tileId).activityid;	
	
	createComment(text,tileId,activityid,addCommentLB_callBack,callBackArguments); 
}

/**
 * to handle comment creation
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */
function addCommentLB_callBack(callBackArguments, result, status, xhr) {
	if (isSuccess(status)) {	
			var allTiles = [];
		$.each(tileLB,function(i, tile) {
							if (callBackArguments.tileId == tile.id) {
								result = validateJSONData(JSON.stringify(result));
								result.isCreator = true;
								tile.comments.splice(0, 0, result);
								tile.comment.push(result);
								var commentdata = {
									"tileid" : tile.id,
									"comments" : tile.comments,
									"tiletype":callBackArguments.typeOfTile
								};
								var comment=[];
								comment.push(result);
								tile.attachmentList = getAttachmentList(result.attachment, null, tile.id);		
								
								var commentResult=new Array();
								commentResult.push(result);	
				
								if(commentResult[0].replytoid&&callBackArguments.typeOfTile=='query'){
				
								var commentTemplate=getTemplate("replyToQueryTemplate");
								var commentData=commentTemplate(commentResult[0]);
								$("#replyToComment_"+commentResult[0].replytoid).append(commentData);	
								}
								else{
								var commentTemplate=getTemplate("commentBox_"+callBackArguments.typeOfTile+"_Template");
								var commentData=commentTemplate(commentResult);
								$('.commentBoxData_'+result.tileid).prepend(commentData);	
								
								var gridViewCommentsTemplate = getTemplate("gridViewCommentsTemplate");
								var gridViewCommentsData = gridViewCommentsTemplate(commentdata);
								$("#comments_" + result.tileid).html(gridViewCommentsData);
								
								var commentcount=parseInt($('.commentCount').html())+1;	
								$('.commentCount').html(commentcount);	
}					
								tile.attachmentList=removeOlderVersions(tile.attachment, tile.comment,tile.id);
								if (attachedIds.length > 0) {
									var attachmentData = {
										"tileid" : tile.id,
										"attachmentList" : tile.attachmentList
									};	
								var lightBoxAttachmentTemplate=getTemplate("lightBox_post_AttachmentTemplate");
								var html=lightBoxAttachmentTemplate(attachmentData.attachmentList);
								
								$("#lightboxattachment_"+tile.id).attr("style","display:block");
								
								$("#lightboxattachment_"+tile.id).html(html);
								
														
								var gridViewAttachmentTemplate = getTemplate("gridViewAttachmentTemplate");
								gridViewAttachmentData = gridViewAttachmentTemplate(attachmentData);
								$("#attachments_" + result.tileid).html(gridViewAttachmentData);
								
								
								
								$("#uploadFileStatusForlghtBox_"+tile.id).html("");			
								$("#uploadFileStatusForlghtBox_"+tile.id).attr("style","display:none");								
								$("#uploadFileStatusForlghtBox_"+callBackArguments.commentId).html("");			
								$("#uploadFileStatusForlghtBox_"+callBackArguments.commentId).attr("style","display:none");																			
									}										
									$("#text_detail_"+tile.id).val("");
									$("#text_detail_"+callBackArguments.commentId).val("");
									$("#lightBox_comment_"+callBackArguments.commentId).attr("style","display:none");									
								}

							
						});
		
	} else {
		$("#uploadFileStatus_"+callBackArguments.tileId).hide();
	}
	attachedIds = [];	
		rearrangeTiles();
	}
/**
 * to return tileid and activityid based on comment id
 * 
 * @param commentId
 */
 
 function getTileAndActivity(commentId){
 var tilenAndActivity=new Object;
 $.each(tileData, function(i, tile) {
		$.each(tile.comments, function(i, comment) {
			if (comment.commentid == commentId){				
				tilenAndActivity.tileId = tile.id;
			  tilenAndActivity.activityid=tile.activityid;
			}
		});
	});
	return tilenAndActivity;
	}
 
/**
 * to return tileid and activityid based on comment id for lightbox
 * @param commentId
 * @returns {___anonymous11124_11139}
 */
 function getTileAndActivityLB(commentId){
	 var tilenAndActivity=new Object;
	 $.each(tileLB, function(i, tile) {
			$.each(tile.comments, function(i, comment) {
				if (comment.commentid == commentId){				
					tilenAndActivity.tileId = tile.id;
				  tilenAndActivity.activityid=tile.activityid;
				}
			});
		});
		return tilenAndActivity;
		}
 
 /**
  * to create coment on comment
  * @param text
  * @param commentId
  * @param tilenAndActivity
  * @param callBackMethod
  * @param callBackArguments
  */

	function createCommentOnComment(text, commentId, tilenAndActivity, callBackMethod, callBackArguments){

		var tileId=tilenAndActivity.tileId;
		var activityid=tilenAndActivity.activityid;
		
		if(text!=null&&text!=""){
			$("#text_"+commentId).removeClass("red");
			removePlaceholderClass("#text_"+commentId);
		var jsondata = JSON.stringify([ {
		tileid : tileId,
		commenttext : text,
		replytoid : commentId,
		attachment : attachedIds,
		createdby : getUserId(),
		activityid:activityid
		} ]);
		var postData = {
			"jsondata" : jsondata
		};
	

		performAJAX(getCreateCommentURL(), "POST", postData, "",
		callBackMethod, callBackArguments,
			"application/x-www-form-urlencoded");
}else{
	$("#text_"+commentId).attr("placeholder","Please enter your reply.");
	$("#text_"+commentId).addClass("red");
addPlaceholderClass("#text_"+commentId);
	showplc();
	
}
}

	/**
	 * to add comment on comment
	 * @param commentId
	 * @param tiletype
	 */
function addCommentOnComment(commentId,tiletype) {
	var tilenAndActivity=getTileAndActivity(commentId);
		var tileId=tilenAndActivity.tileId;
	var text = $("#text_" + commentId).get(0).value;
	text = text.replace(/(\r\n|\n|\r)/gm, "<br>");
	

	
		var callBackArguments = {
			"tileId" : tileId,
			"tiletype":tiletype
		};
	
	createCommentOnComment(text,commentId,tilenAndActivity,addComment_callBack,callBackArguments);	
}

/**
 * function addcomment on comment from lightbox
 * @param commentId
 * @param typeOfTile
 */
function addCommentOnCommentLB(commentId,typeOfTile) {
	var tilenAndActivity=getTileAndActivityLB(commentId);
		var tileId=tilenAndActivity.tileId;
	var text = $("#text_detail_"+commentId).get(0).value;
	text = text.replace(/(\r\n|\n|\r)/gm, "<br>");
	$("#text_detail_"+commentId).val("");

	
		var callBackArguments = {
			"tileId" : tileId,
			"typeOfTile":typeOfTile,	
			"commentId":commentId		
		};
	
	createCommentOnComment(text,commentId,tilenAndActivity,addCommentLB_callBack,callBackArguments);	
}
/*** 
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 * to handle comment callback
 */
function addComment_callBack(callBackArguments, result, status, xhr) {
	if (isSuccess(status)) {	
		var allTiles = [];
		tileData=customiseTileData(tileData);
		$
				.each(
						tileData,
						function(i, tile) {
							if (callBackArguments.tileId == tile.id) {
								result = validateJSONData(JSON
										.stringify(result));
								result.isCreator = true;
								tile.comments.splice(0, 0, result);
								tile.comment.push(result);
								var commentData = {
									"tileid" : tile.id,
									"comments" : tile.comments,
									"tiletype":callBackArguments.tiletype
								};
								var comment=[];
								comment.push(result);
								tile.attachmentList = getAttachmentList(
										tile.attachment, tile.comment,tile.id);
                          
								tile.attachmentList=removeOlderVersions(tile.attachment, tile.comment,tile.id);
								
								var gridViewCommentsTemplate = getTemplate("gridViewCommentsTemplate");
								var html = gridViewCommentsTemplate(commentData);
								$("#comments_" + tile.id).html(html);

								if (attachedIds.length > 0) {
									var attachmentData = {
										"tileid" : tile.id,
										"attachmentList" : tile.attachmentList
									};
							
									var gridViewAttachmentTemplate = getTemplate("gridViewAttachmentTemplate");
									var html = gridViewAttachmentTemplate(attachmentData);
								
									$("#attachments_" + tile.id).html(html);
								}

							}
						});

	} else {
		
		$("#uploadFileStatus_"+callBackArguments.tileId).hide();
	}

	attachedIds = [];
	rearrangeTiles();
}

/**
 * customise tileData array 
 * @param tileData
 * @returns
 */


function customiseTileData(tileData) {
	var userid=getUserId();
	var tiledata = getDataIntoNewVariable(tileData);
	var tempTileData = new Array();
	$.each(tiledata,
			function(i, tile) {				
				var tempTile = copyAttributesToNewVariable(tile);
				tempTile.isNotBookmarked = true;
				for(var i=0;i<bookMarkedTilesArray.length;i++){
					if(bookMarkedTilesArray[i]==tile.id){
						tempTile.isNotBookmarked = false;
						break;
					}
				}
			
				 tempTile.author=tile.author.initCap();
				if(tempTile.teaser!=null){
				tempTile.teaser=tile.teaser.replace(/<br\s*\/?>/mg,"\r\n");
				tempTile.teaser=capitalizeFirstLetter(tempTile.teaser);
				}
				tempTile.title=tile.title.replace(/<br\s*\/?>/mg,"\r\n");
				tempTile.title=capitalizeFirstLetter(tempTile.title);
				if(tile.status=='CLOSE'){
					tempTile.action='Reopen';
					tempTile.isClosed=true;
				}
				else
				tempTile.action='Close';	
				tempTile.attachmentList = getAttachmentList(tile.attachment,
						null,tile.id);
				tempTile.isSingleUserTask=isSingleUserTile(tile);
				tempTile.isCreator=isInitiator(tile.authorid);
				tempTile.isTaskAdded=isTaskAssigned(tile.recipients);	
				tempTile.activityname=getSelectedActivityName(tile.activityid);
			    tempTile.comment=[];
			    if(tile.tiletype=='post'){
			    	   tempTile.classNames="creat-new-post glyphicon glyphicon-pencil";	
			    }
			    else if(tile.tiletype=='task'){
			    	   tempTile.classNames="creat-new-task fa fa-check-square-o";	
			    }
			    else if(tile.tiletype=='query'){
			    	   tempTile.classNames="creat-new-query fa fa-question-circle fa-2";	
			    }
			    if(tile.avatar==""||tile.avatar==null||tile.avatar==undefined){
			    	tempTile.isAvatarFound=false;
			    }
			    else{
			    	tempTile.isAvatarFound=true;
			    }
			  if(tile.tiletype=='task'&&tile.data!=null&&tile.data!=""&&tile.data!=undefined){
			    tempTile.response= getResponse(tile.data.attributeMaps);
			   var progress=getAssigneeValue("progress", tile.data.attributeMaps,userid)
			   if(progress!=null)  
			      tempTile.progress=progress/10;
			 var userResponse=getAssigneeValue("response", tile.data.attributeMaps,userid)
			if(userResponse=='accepted'||userResponse=='needhelp'){
				tempTile.userResponse=true;
			}
			else{
				tempTile.userResponse=false;
			}
				
			  }
			    else{
				  tempTile.response=false;
				  tempTile.progress=null;
				  tempTile.userResponse=false;
			    }
			 for(var i=0;i<tempTile.recipients.length;i++){
				 for(var j=0;j<activityMembers.length;j++){
					 if(tempTile.recipients[i].recipientid==activityMembers[j].id){
						 tempTile.recipients[i].email=activityMembers[j].email;
						 tempTile.recipients[i].avatar=activityMembers[j].avatar;
					 }
				 }
			 }
			  
			  tempTileData.push(tempTile);
			});	
	
	return tempTileData;
	
}











/**
 * 
 * to find user responded on task 
 * 
 * @param attributeMaps
 * @returns {Boolean}
 */

function getResponse(attributeMaps){
	var userid=getUserId();
	var response=getTaskProgress(attributeMaps,userid);
	if(response)
		return true;
	else
		return false;
	
}


/**
 * to load tiles in list view
 * @param tileData
 */

function loadTilesInListView(tileData) {
	tileData=customiseTileData(tileData)
	var listViewTileTemplate = getTemplate("listViewTileTemplate");
	var html = listViewTileTemplate(tileData);
	$("#list").attr("class", "active");
	$("#grid").attr("class", "grid");
	
	if ($("#showTilesOrNotifications").find(".scroles").get(0) == undefined){
		$("#showTilesOrNotifications").html(
				"<div class='scroles scrib_container'><div class='scrib-margin-list'> " + html + "</div></div>");
	
		var listViewTileTemplate = getTemplate("listViewTileTemplate");
		var html = listViewTileTemplate(tileData);
	}
	else{
		var listViewTileTemplate = getTemplate("listView");
		var html = "";
		$.each(tileData,function(i,tile){
			if(i==0){
				html=listViewTileTemplate(tile);
			}
			else{
				html=html+listViewTileTemplate(tile);
			}
		});
		$("#list_view").append(html);
	}
}

/**
 * to load tiles in tileview
 * @param tileData
 */
function loadTilesInTileView(tileData) {	
	tileData=customiseTileData(tileData);
	var gridViewTemplate = getTemplate("gridViewTemplate");
	var html = gridViewTemplate(tileData);
	
	$("#grid").attr("class", "active");
	$("#list").attr("class", "list");
	
	
	if ($("#showTilesOrNotifications").find(".scroles").get(0) == undefined)
		$("#showTilesOrNotifications").html(
				"<div class='scroles scrib_container' >" + html + "</div>");
	else
		$("#showTilesOrNotifications").find(".scroles").append(html);
}


/**
 * to add to book marks
 * @param tileId
 */
function addTileToPod(tileId,tileType) {
	if (isValueNotEmpty(tileId)) {
		var url = getAddToPodURL(tileId,activityType);
		var callBackArguments={
				"tileId":tileId,
				"tileType":tileType
		}
		performAJAX(url, "GET", "", "", addTileToPod_callBack,callBackArguments, "");
	}
}

/**
 * to handle add bookmarked tiles 
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */
function addTileToPod_callBack(callBackArguments, result, status, xhr) {
	showAlert();
	if (status == SUCCESS){
		$("#tile_bookmark_"+callBackArguments.tileId).attr("onclick","javascript:deleteTileFromPod('"+callBackArguments.tileId+"','"+callBackArguments.tileType+"')");
		$("#bookmark_"+callBackArguments.tileId).html("Un-Bookmark");
		$(".success").html(callBackArguments.tileType.initCap()+"   added to your bookmark's");
		$(".lightbox_bookmark_"+callBackArguments.tileId).html("<h6 onclick=javascript:deleteTileFromPod('"+callBackArguments.tileId+"','"+callBackArguments.tileType+"')>Unbookmark</h6>");
		loadBookMarkedTiles();
	}
	else{
		if(result=='Gone'){
		$(".success").html(" this tile is already book marked");

		}
		}
}

/*******************************************************************************
 * unbook marked tiles
 * @param tileId
 */
function deleteTileFromPod(tileId,tileType) {
	if (isValueNotEmpty(tileId)) {
		var callBackArguments={
				"tileId":tileId,
				"tileType":tileType
		}
		var url = getDeleteFromPodURL(tileId);
		
		performAJAX(url, "GET", "", "", deleteTileFromPod_callBack,
				callBackArguments, "");
	}
}

/*******************************************************************************
 * delete pod handling
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */
function deleteTileFromPod_callBack(callBackArguments, result, status, xhr) {
	showAlert();
	if (status == SUCCESS){
		
		$("#tile_bookmark_"+callBackArguments.tileId).attr("onclick","javascript:addTileToPod('"+callBackArguments.tileId+"','"+callBackArguments.tileType+"')");
		$("#bookmark_"+callBackArguments.tileId).html("Bookmark");
		$(".lightbox_bookmark_"+callBackArguments.tileId).html("<h6 onclick=javascript:addTileToPod('"+callBackArguments.tileId+"','"+callBackArguments.tileType+"')>Bookmark</h6>");
		$(".success").html(callBackArguments.tileType.initCap()+"   removed from your bookmark's ");
	if(starred){
		$("#tile_"+callBackArguments.tileId).remove();
		$("#list_"+callBackArguments.tileId).remove();
	}
		loadBookMarkedTiles();
		rearrangeTiles();
	}else{
		$(".success").html(" failed to unbookmark");

	}
}

/**
 * to handle actions on tasks
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */
function performAction_callBack(callBackArguments, result, status, xhr) {
	if (isSuccess(status)) {
		$("#tileActions_"+callBackArguments.tileId).html(showMore(callBackArguments.tileId,'task',true));
		if(!tileTobeCreated){
			reLoadSmartList(getListId());
		}
		}
	showAlert();
	$(".success").html(callBackArguments.successMessage);
	repaintListViewTile(callBackArguments);
	if(view=='gridview'){
     rearrangeTiles();
	}
}

/**
 * to repaint the list view after actions
 * 
 * @param status
 */


function repaintListViewTile(status){
	var tempTile=[];
	tileData=customiseTileData(tileData);
	$.each(tileData, function(i, tile) {
		if(status.tileId==tile.id)
			tempTile=tile;
	});

	if(status.response=="Accepted"||status.response=="Needhelp"){
		tempTile.response=true;
		tempTile.progress=0;
		  tempTile.isClosed=false;
		  tempTile.userResponse=true;
	}
	else{
		tempTile.response=false;
		tempTile.progress=0;
		  tempTile.isClosed=false;
		  tempTile.userResponse=false;
	}
	if(tempTile.isTaskAdded=="true"&&tempTile.response==true){
	var ListViewTemplate = getTemplate("ListViewTemplate");
	html = ListViewTemplate(tempTile);
	

	var responseStatusTemplate=getTemplate("lightBox_statusTemplate");
	var responseStatus=responseStatusTemplate(tempTile);
	
	$("#lightBoxStatusUpdate_"+status.tileId).html(" "+responseStatus);						
	
	
	

	$("#prog_list_"+status.tileId).html(html);
	$("#list_progress_"+status.tileId).attr("style","display:none");
	$('#status_'+status.tileId).text(" "+status.response);
	$('#status_lightbox_'+status.tileId).text(" "+status.response);
	
	}
	 if(status.response=="Rejected"){
		$(".task_list_"+status.response+"_"+status.tileId).attr("style","display:none");
		$("#status_"+status.tileId).text(" "+status.response);	
		$("#status_lightbox_"+status.tileId).text(" "+status.response);	
}
	if(status.response=="Updated"){
		$("#status_"+status.tileId).text(" "+status.progress+"% completed");
		$("#status_lightbox_"+status.tileId).text(" "+status.progress+"% completed");
		$(".range_"+status.tileId).val(parseInt(status.progress/10));
	}
}


/**
 * to load book marked tiles
 */
function loadStarredTiles() {
	$(window).unbind('scroll');
	showBookMarksHeader();
	
	$(".scrib_slidedown").hide();
	$("#dyna_dashboard").hide();
	$("#dyna_mainContent").show();
	
	tiletype="All";
	makeActivityActive(tiletype);
	makeTileTypeActive(activity,tiletype);
	
	isFilteredTiles=false;
	$("#showTilesOrNotifications").find(".scroles").remove();
	$(".scrib_slidedown").find(".scroles").remove();
	performAJAX(getMyPodURL(activityType), "GET", "", "", loadStarredTiles_callBack, "",
			"application/x-www-form-urlencoded");
}

/**
 * to load bookmarked tiles and painting
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */
function loadStarredTiles_callBack(callBackArguments, result, status, xhr) {
	var currentUrl=window.location.href;
	 currentUrl=currentUrl.substring(0,currentUrl.indexOf('#'));
	 window.location.href=currentUrl+"#bookmarks";
	var data = customiseTileData(result);
	$.each(data, function(i, tile) {
		tile.isNotBookmarked = false;
	
	});
tileData=data;
	
	if (isSuccess(status)) {
		starred = true;
		if (result.length != 0) {
			
			if (view  == 'gridview') {
				
				loadTilesInTileView(data);
			
				rearrangeTiles();
			} else {
				loadTilesInListView(data);
			}
			
		}else{
			$("#showTilesOrNotifications").html(
			"<div class='scrib_container no_res'> Oops!! Nothing here </div>");
		}
	}

}

/**
 * to get attachments from comments and tiles
 * @param attachments
 * @param comments
 * @returns
 */
function getAttachmentList(attachments, comments,id) {
	var attachmentsList = [];

	if (attachments != null && attachments!=[]) {
		$.each(attachments, function(i, attach) {
			var attachment = new Object();
			attachment.filePath = attach;
			attachment.fileName = attach.substring(attach.lastIndexOf("/") + 1,
					attach.length);
			attachment.fileType = attach.substring(attach.lastIndexOf(".") + 1,
					attach.length);
			if(attach.lastIndexOf("(V")>0){
				var fileName = attach.substring(attach
						.lastIndexOf("/")+1, attach.length);
				attachment.fileName=fileName.replace(fileName.substring(fileName.lastIndexOf("(V"),fileName.lastIndexOf(")")+1),"");
				}
			attachment.attachmentId=i+"attach"+id;
			attachment.id = id;
			attachmentsList.push(attachment);
		});
	}
	if (comments != null) {
		$.each(comments, function(i, comment) {
			if (comment.attachment != null)
				$.each(comment.attachment, function(j, attach) {
					var attachment = new Object();
					attachment.filePath = attach;
					attachment.fileName = attach.substring(attach
							.lastIndexOf("/") + 1, attach.length);
					attachment.fileType = attach.substring(attach
							.lastIndexOf(".") + 1, attach.length);
					attachment.createddate = comment.createddate;
			
					
					if(attach.lastIndexOf("(V")>0){
						var fileName = attach.substring(attach
								.lastIndexOf("/")+1, attach.length);
						attachment.fileName=fileName.replace(fileName.substring(fileName.lastIndexOf("(V"),fileName.lastIndexOf(")")+1),"");
						}
					attachment.attachmentId=i+"attach"+id;
					attachment.id = id;
					attachmentsList.push(attachment);

				});
		});
	}
	return attachmentsList;
}

/*******************************************************************************

/*** 
 * 
 *to find duedate from data attributes
 * @param object
 * @param givenKey
 * @returns
 */
function getDueDateFromDataAttributes(object, givenKey) {
	var givenKeyValue = "";
	$.each(object, function(key, value) {
		if (key == givenKey)
			givenKeyValue = value;
	});
	return showDueDate(givenKeyValue);
}

Handlebars.registerHelper('getDueDate', function(object, givenKey) {
	return new Handlebars.SafeString(getDueDateFromDataAttributes(object,
			givenKey));
});


Handlebars.registerHelper('getStartDate', function(givenDate) {
	return showCustomDate(givenDate);
});


/**
*to get filetype image
* @param filePath
* @param fileType
* @returns
*/
function fileTypeImage(filePath,fileType,type,needThumbNail) {   
   var attachmentPath = "";
   fileType=filePath.substring(filePath.lastIndexOf('.')+1,filePath.length);  
   if (fileType == 'gif')
       attachmentPath = "serv/afile?filepathtype="+type+"&name=" + filePath;
   else if (fileType == 'jpg')
       attachmentPath = "serv/afile?filepathtype="+type+"&name=" + filePath;
   else if (fileType == 'jpeg')
       attachmentPath = "serv/afile?filepathtype="+type+"&name=" + filePath;
   else if(fileType=='png')
       attachmentPath = "serv/afile?filepathtype="+type+"&name=" + filePath;   
    else if ((fileType == 'doc'||fileType == 'docx'||fileType == 'xls'||fileType == 'xlsx'
	   ||fileType == 'ppt'||fileType == 'pptx'||fileType == 'js'||fileType == 'txt'||fileType == 'pdf'||fileType == 'html')&&needThumbNail)
       attachmentPath = "serv/afile?name="+ filePath+"_thumbnail.jpg";
 else if (fileType == 'docx'||fileType == 'doc')
       attachmentPath = "assets/images/Word-2013.png";
   else if (fileType == 'xls')
       attachmentPath = "assets/images/Excel-2013.png";
   else if (fileType == 'xlsx')
       attachmentPath = "assets/images/Excel-2013.png";
   else if (fileType == 'ppt')
       attachmentPath = "assets/images/Powerpoint-2013.png";
   else if (fileType == 'pptx')
       attachmentPath = "assets/images/Powerpoint-2013.png";
   else if (fileType == 'zip'||fileType == 'gz'||fileType == 'tar'||fileType == 'jar')
       attachmentPath = "assets/images/rar.png";
   else if (fileType == 'js')
       attachmentPath = "assets/images/js.png";
   else if(fileType == 'txt')
	   attachmentPath = "assets/images/txt.png";
   else if(fileType == 'pdf'){
	   attachmentPath = "assets/images/pdf.png";
   }
   else if(fileType=='css'){
	   attachmentPath = "assets/images/css.png";
   }
   else if(fileType == 'mp3'||fileType == 'MP3'||fileType == 'mp4'||fileType == 'flv'||fileType == 'wmp'||fileType == 'avi'||fileType == 'MPEG'){
	   attachmentPath = "assets/images/video.png";
   }
   else if(fileType=='html')   {
	   attachmentPath = "assets/images/html.png";
   }
   else if(fileType=='php')   {
	   attachmentPath = "assets/images/php.png";
   }
   
   else if(fileType=='psd')   {
	   attachmentPath = "assets/images/psd.png";
   }
   else
       attachmentPath = "assets/images/unknown.png";
   return attachmentPath;
}

Handlebars.registerHelper('fileTypeImage', function(filePath, fileType,type,needThumbNail) {
   return new Handlebars.SafeString(fileTypeImage(filePath, fileType,type,needThumbNail));
});



/**
 * 
 * @param numberOfComments
 * @param id
 * @returns
 */


Handlebars.registerHelper('showCommentsList', function(comments,tiletype) {
	var tempComments = new Array();
	for (i = 0; i < comments.length; i++) {
		comments[i].commenttext=comments[i].commenttext.replace(/<br\s*\/?>/mg,"\r\n");
		comments[i].tiletype=tiletype;
		tempComments[i] = comments[i]
	}
	var commentTemplate = getTemplate("commentTemplate");
	var html = commentTemplate(tempComments);
	return new Handlebars.SafeString(html);
});

/**
 *to display task actions
 *  
 * @param tileId
 * @param authorId
 * @param attributeMaps
 * @returns {String}
 */

function showTaskActions(tileId, authorId, attributeMaps) {
	var sb = "";
	var actionStatus = true;
	var isTaskAdded="";
	tileData=customiseTileData(tileData);
	$.each(tileData,function(i,tile){
		if(tile.id==tileId){
			isTaskAdded=tile.isTaskAdded;
		}
});
	if (isTaskAdded=="true"){
		var actionStatus=getTaskActions(attributeMaps,authorId);	
	}
	if (actionStatus)
		return sb;
	else {
		
		sb+="<span id='task_Accepted_"
				+ tileId+"'class='scrib-labels pull-left scrib-link-over'  onclick=javascript:performAction('"
				+ tileId + "','Accepted')><strong>Accept</strong></span>";
		
		sb+="<span class='scrib-labels pull-left scrib-link-over' id='task_Rejected_"
				+ tileId +"'  onclick=javascript:performAction('"
			+ tileId + "','Rejected')><strong>Reject</strong></span>";
		
		sb+="<span id='task_needhelp_"
				+ tileId +"'class='scrib-labels pull-left scrib-link-over'   onclick=javascript:performAction('"
			+ tileId + "','Needhelp')><strong>Need Help</strong></span>";
	
		return sb;
	}
	

}

Handlebars
.registerHelper(
		'showTaskActionsListView',
		function(tileId,authorId,attributeMaps,isTaskAdded) {
			var sb="";
		if (isTaskAdded=="true"){
			var actionStatus=getTaskActions(attributeMaps,authorId);
			
			if (actionStatus)
				return new Handlebars.SafeString(sb);
			else {	
				sb += "<span class='scrib-lmargin text-muted scrib-link-over task_list_Accepted_"+tileId+"'  onclick=javascript:performAction('"+ tileId + "','Accepted') id='task_list_Accepted_"+tileId+"'><strong>Accept</strong></span>";
				sb += "<span class='scrib-lmargin text-muted scrib-link-over task_list_Rejected_"+tileId+" '  onclick=javascript:performAction('"+ tileId + "','Rejected') id='task_list_Rejected_"+tileId+"' ><strong>Reject</strong></span>";
				sb += "<span class='scrib-lmargin text-muted scrib-link-over task_list_Needhelp_"+tileId+"'  onclick=javascript:performAction('"+ tileId + "','Needhelp') id='task_list_Needhelp_"+tileId+"' ><strong>Need Help</strong></span>";
				return new Handlebars.SafeString(sb);
			}
		}else
			return new Handlebars.SafeString(sb);

		});



/**
 * to get user response
 * 
 * @param aKey
 * @param attributeMaps
 * @param userId
 * @returns
 */
function getAssigneeValue(aKey, attributeMaps,userId){
	var returnValue="";
	if(attributeMaps!=null&&attributeMaps!=undefined&&attributeMaps!=""){
	var userList =  getOuterAttributeMapList("assignees", attributeMaps);
	if(userList==null) {
		return null;
	}
	var userMap = getInnerAttributeMapList(userId, userList);
	if(userMap==null){
		return null;
	}
	 returnValue  = getAttributeValue(aKey, userMap);
	 if(returnValue==null||returnValue==undefined)
		 returnValue="";
	}
	
	return returnValue;
	
}

/**
 * to get value from innermaps list
 * @param aKey
 * @param innerMapList
 * @returns
 */
function getAttributeValue(aKey, innerMapList){
	var result = null;
	$.each(innerMapList, function(i, innerMap) {
		$.each(innerMap, function(key, value) {
			if (key == aKey) {
				result = value;
				return;
			}
		});
	});
	return result;

}
/**
 * to get innermap from outermap
 * @param aKey
 * @param outerAttributeMapList
 * @returns
 */
function getInnerAttributeMapList(aKey, outerAttributeMapList){
	var result = null;
	$.each(outerAttributeMapList, function(i, innerAttMap) {
		$.each(innerAttMap, function(key, value) {
			if (key == aKey) {
				result = value;
				return;
			}
		});
	});
	return result;	
	
}

/**
 * 
 * to get outer attributemaps list from attributemaps
 * @param aKey
 * @param attributeMaps
 * @returns
 */

function getOuterAttributeMapList(aKey, attributeMaps){
	var result = null;
	$.each(attributeMaps, function(i, attributeMap) {
		$.each(attributeMap, function(key, value) {
					if (key == aKey) {
						result = value;
						return;
					}
		});
		});
	
	return result;
}

/**
 * to find out the status from attributemaps
 * @param attributeMaps
 * @param authorid
 * @returns {Boolean}
 */

function getTaskActions(attributeMaps,authorid){
	var status=true;
	var userid=getUserId();
	if(authorid!=userid){
	status=getTaskProgress(attributeMaps,userid);
	}
	return status;
		
}


/**
 * to find out the user response
 * 
 * @param attributeMaps
 * @param userid
 * @returns {Boolean}
 */

function getTaskProgress(attributeMaps,userid) {
	var actionStatus = true;
	var assigneeValue=null;
    assigneeValue = getAssigneeValue("response", attributeMaps,userid);
		if(assigneeValue!=null && assigneeValue != ""){
			actionStatus = true;
		}else {
			actionStatus = false;
		}
     return actionStatus;	
}

/*******************************************************************************/
 /**
  * to perform actionson tasks
  *  @param tileId
 * @param actionType
 */
function performAction(tileId, actionType) {

	var response="Updated";
	updateTileTemplate.tileid = tileId;
	updateTileTemplate.updatedby = getUserId();
	var mapArray = [];
	var map = {};

	var actions = [];
	if (actionType == 'Accepted'){
		actionTemplate.response = "accepted";
		progressTemplate.progress = "0";
		response="Accepted";
		successMessage="Task  accepted";
		actions.push(actionTemplate);
	}
	else if (actionType =='Rejected'){
		actionTemplate.response = "rejected";
		response="Rejected";
		progressTemplate.progress = "0";
		successMessage="Task  rejected";
		actions.push(actionTemplate);
	}
	else if (actionType == 'Needhelp'){
		actionTemplate.response = "needhelp";
		progressTemplate.progress = "0";
		response="Needhelp";
		successMessage="Your request for help is registered";
		actions.push(actionTemplate);
		
	}
	else 
		{
		progressTemplate.progress = actionType*10+"";
		actions.push(progressTemplate);
		successMessage="Task updated ";
		}	

	map[userId] = actions;
	mapArray.push(map);
	attributeMapTemplate.assignees = mapArray;

	var assigneesArray = [];
	assigneesArray.push(attributeMapTemplate);
	updateTileTemplate.attributesToBeChanged = null;
	updateTileTemplate.attributeMapsToBeChanged = assigneesArray;

	var postData = {
		"jsondata" : JSON.stringify(updateTileTemplate)
	};
	var callbackData = {
		"response" : "",
		"tileId" : "",
		"userId" : "",
		"successMessage":successMessage
	};
	callbackData.response = response;
	callbackData.tileId = tileId;
	callbackData.userId = userId;
	if(	progressTemplate.progress!=0)
		callbackData.progress=actionType*10;
	else
		callbackData.progress=0;
	performAJAX(getUpdateTileURL(), "POST", postData, "",performAction_callBack, callbackData,
			"application/x-www-form-urlencoded");
}
/**
 * to returns header of tile
 * 
 */
Handlebars.registerHelper('showHeader', function(avatar, author, createddate,
		tiletype, id,title,activityid,data,isSingleUserTask,authorid,activityassigned,classNames) {
	var data = {
		"avatar" : avatar,
		"author" : author,
		"createddate" : createddate,
		"tiletype" : tiletype,
		"id" : id,
		"title":title,
		"activityid":activityid,
		"data":data,
		"isSingleUserTask":isSingleUserTask,
		"authorid":authorid,
		"activityassigned":activityassigned,
		"classNames":classNames
	};
	var gridViewHeaderTemplate = getTemplate("headerTemplate");
	var html = gridViewHeaderTemplate(data);
	return new Handlebars.SafeString(html);
});

/**
 * to returns body of tile
 * 
 * 
 */

Handlebars.registerHelper('showBody', function(teaser, tiletype, data,
		activityid,authorid,tileId,attachmentList,isNotBookmarked,recipients,tags,status) {
	
	var data = {
		"teaser" : teaser,
		"tiletype" : tiletype,
		"data" : data,
		"activityid" : activityid,
		"authorid":authorid,
		"id":tileId,
		"attachmentList":attachmentList,
		"isNotBookmarked":isNotBookmarked,
		"recipients":recipients,
		"tags":tags,
		"status":status
	};
	var gridViewBodyTemplate = getTemplate("detailsTemplate");
	var html = gridViewBodyTemplate(data);
	return new Handlebars.SafeString(html);
});
/**
 * to return attachments
 * 
 */

Handlebars.registerHelper('showAttachments', function(id, attachmentList) {
	var data = {
		"tileid" : id,
		"attachmentList" : attachmentList
	};
	var gridViewAttachmentTemplate = getTemplate("gridViewAttachmentTemplate");
	var html = gridViewAttachmentTemplate(data);
	return new Handlebars.SafeString(html);
});


Handlebars.registerHelper('showAttachment', function(attachment) {
	var attachmentTemplate = getTemplate("attachmentTemplate");
	var html = attachmentTemplate(attachment);
	return new Handlebars.SafeString(html);
});
/************************************************************************************/

/**
 * to return comments
 * 
 */

Handlebars.registerHelper('showComments', function(comments, id,tiletype) {
	var isCreator = "";
	if(comments!=null && comments!=[])
	for (i = 0; i < comments.length; i++) {
		if (comments[i].authorid == getUserId())
			comments[i].isCreator = true;
		else
			comments[i].isCreator = false;
	}
	return showComments(comments, id,tiletype);
});

/**
 * to show comment box
 */

Handlebars.registerHelper('showCommentBox', function(id, isTileId,isLightBox,typeOfTile) {
	if(isLightBox){	
		var data = {
				"id" : id,
				"isTileId" : isTileId,
				"typeOfTile":typeOfTile
			};
		var commentBoxTemplate = getTemplate("lightBox_commentBoxTemplate");
		var html = commentBoxTemplate(data);
		return new Handlebars.SafeString(html);
	}
	else{		
		var data = {
				"id" : id,
				"isTileId" : isTileId,
				"tiletype":typeOfTile
			};
		var commentBoxTemplate = getTemplate("commentBoxTemplate");
		var html = commentBoxTemplate(data);
		return new Handlebars.SafeString(html);
	}
});

/**
 * to show comments
 * @param comments
 * @param id
 * @param tiletype
 * @returns {Handlebars.SafeString}
 */
function showComments(comments, id,tiletype) {
	var data = {
		"tileid" : id,
		"comments" : comments,
		"tiletype":tiletype
	};
	var gridViewCommentsTemplate = getTemplate("gridViewCommentsTemplate");
	var html = gridViewCommentsTemplate(data);
	return new Handlebars.SafeString(html);
}

/**
 * to loadtiles
 * @param viewType
 * @param activityId
 * @param tileType
 */
function loadTiles(viewType, activityId, tileType) {
	var lastupdateddate = null;
	var latestTileIds = null;
	var postData="";
	if(isFilteredTiles){
		postData=filterValues;
	}
	view = viewType;
	tiletype = tileType;
	activity = activityId;
	var callBackArguments = {
		"activityId" : activityId,
		"viewType" : viewType,
		"tileType" : tileType
	};
	if (tileData.length != 0) {
		for (i = 0; i < tileData.length; i++) {
			if (latestTileIds == null)
				latestTileIds = tileData[i].id;
			else
				latestTileIds = latestTileIds + "~" + tileData[i].id;
		}
		lastupdateddate = tileData[tileData.length - 1].lastupdateddate;
	}
	if (activityId == HOME_ID) {
		performAJAX(getActivityTilesForPaginationURL("home",recordsForScroll,lastupdateddate, latestTileIds,
				tileType,getUserId()), "POST", postData, "", loadTiles_callBack,
				callBackArguments, "application/x-www-form-urlencoded");
	} else {
	
		performAJAX(getActivityTilesForPaginationURL(activityId,recordsForScroll,lastupdateddate, latestTileIds,
				tileType,getUserId()), "POST", postData, "", loadTiles_callBack,
				callBackArguments, "application/x-www-form-urlencoded");	 
	}
}

function loadtilesFromDashboard(viewType, activityId, tileType) 
	{
	var lastupdateddate = null;
	var latestTileIds = null;
	var postData="";
	if(isFilteredTiles){
		postData=filterValues;
	}
	view = viewType;
	tiletype = tileType;
	activity = activityId;
	var callBackArguments = {
		"activityId" : activityId,
		"viewType" : viewType,
		"tileType" : tileType
	};
	performAJAX(getActivityTilesForPaginationURL(activityId,recordsForScroll,lastupdateddate, latestTileIds,
				tileType,getUserId()), "POST", postData, "", loadTiles_callBack,
				callBackArguments, "application/x-www-form-urlencoded");	 
	}
	
/*** 
 * to show tiles in tile or listview
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */
function loadTiles_callBack(callBackArguments, result, status, xhr) {
	$.each(result, function(i, tile) {
		tileData.push(tile);
	});
	if((tileData==[]||tileData==""||tileData==null)){
		$("#showTilesOrNotifications").html(
				"<div class=' scrib_container no_res'> Oops!! Nothing here </div>");
	}
	else{
	if (callBackArguments.viewType == 'gridview') {
		loadTilesInTileView(customiseTileData(result));	
		rearrangeTiles();
	} else {		
		loadTilesInListView(customiseTileData(result));		
	}
	}
	$(window).unbind(
	'scroll');
	if(result.length>=recordsForScroll-1){
	initiatePaginationOnScroll(callBackArguments.viewType,
			callBackArguments.activityId, callBackArguments.tileType);
	}
	$( window ).resize();
}

/*******************************************************************************
 * @param tileType
 * to load tiletype specific tiles
 */
function loadSpecificTiles(tileType) {
	$(window).scrollTop(0);
	updatesCount=0;
	time=null;
	//toggleNav();
    $('.leftbar').removeClass('show-nav');
    $("#scrib-dash-set").html("");
	$("#ActivitySettings_dashboard").hide();
	 $("#info_block").attr("style","display:none");
		$(document).prop('title', "ScribLeaf");
	$(window).unbind('scroll');
	$(".ac-"+activity).prop("checked","true");
	makeActivityActive("all");
	makeTileTypeActive(activity,tileType);
	if(tileType === 'leaveform'){
		tiletype="leaveform";
		loadLeaveform();
		return;
	}
	if(tileType === 'recruitmentform'){
		tiletype="recruitmentform";
		loadRecruitmentform();
		return;
	}
	if(tileType === 'expensereport'){
		tiletype="expensereport";
		loadExpensereport();
		return;
	}
	if(tileType === 'dashboard') {
		tiletype="dashboard";
		loadDashboard();
		return;
	}
		$("#dyna_leaveform").hide();
		$("#dyna_recruitmentform").hide();
		$("#dyna_expensereport").hide();
	
	$("#dyna_dashboard").hide();
	$("#dyna_mainContent").show();
    starred = false;
	isFilteredTiles=false;
$(".scrib_slidedown").hide();
	if(tileType=='post' || tileType=='task' || tileType=='query'){
	tileData = [];
	showCommonHeader();
	$("#dyna_leaveform").hide();
	$("#dyna_recruitmentform").hide();
	$("#dyna_expensereport").hide();
	removeLink("All");
	addLink("Assigned","getAssignedTiles()");
	addLink("BookMarked","loadStarredTiles()");
	$("#showTilesOrNotifications").find(".scroles").remove();
	loadTiles(view, getSelectedActivityId(), tileType);
	}

	if(tileType === 'members'){
		if(accountType != 'Community'){
			loadOrganizationMembers(HOME_ID);
		}
		$("#dyna_leaveform").hide();
		$("#dyna_recruitmentform").hide();
	$("#dyna_expensereport").hide();
		loadActivityMembers(activity,"toDisplayActMems");
		tiletype="members";
		showActvityMemsHeader();
		showActivityMembers(false);
	}

	if(tileType === 'drive'){	
		
		tiletype=tileType;
		performWikiSearch();
		$("#dyna_leaveform").hide();
		$("#dyna_recruitmentform").hide();
	$("#dyna_expensereport").hide();

	
	}

	if(tileType === 'smartlist'){	
		
		tiletype=tileType;
		showSmartList()
		$("#dyna_leaveform").hide();
		$("#dyna_recruitmentform").hide();
		$("#dyna_expensereport").hide();
	}
}


function loadMyleaves(){
	$("#dyna_mainContent").hide();
	$("#dyna_leaveform").show();
	$("#dyna_recruitmentform").hide();
	$("#dyna_expensereport").hide();
	$("#dyna_dashboard").hide();
	
	var temp={};
	temp["userid"] = userData["id"];
	temp["appid"] = "app_1";
  	var getData = {"jsondata" : JSON.stringify(temp)};
	contentType = "application/x-www-form-urlencoded",
	performAJAX( "wfserv/getmyrequests", "POST",getData,"",listofappliedleaves_callback,"",contentType);


}

function listofappliedleaves_callback(callBackArguments, result, status, xhr){

	console.log("applied leaves");
	$("#Leaves").hide();
  	$('#appliedLeaves').show();

    console.log(result);
    var obj = JSON.parse(result);
    if(obj.length==0){
    	console.log("no leaves");
    	$("#appliedLeaves").html(
		"<div class='scrib_container no_res' style='padding-left:225px;'> Oops!! Nothing here </div>");
  
    }
    else{
    	console.log(obj);
    	console.log(obj[0]["appdata"]);
   		leaveappid=obj[0]["appid"];
  		var trail = getTemplate("appliedleaveForms");
  		var htmls = trail(obj);
  		$('#appliedLeaves').html(htmls);
  	} 
}



function loadLeaveform(){
	$("#dyna_mainContent").hide();
	$("#dyna_leaveform").show();
	$("#dyna_recruitmentform").hide();
	$("#dyna_expensereport").hide();
	$("#dyna_dashboard").hide();
	console.log("loadleaveforms");
	var temp = {};
	temp["userid"] = userData["id"];
	temp["appid"] = "app_1";
  	var getData = {"jsondata" : JSON.stringify(temp)};
	contentType = "application/x-www-form-urlencoded",
	performAJAX( "wfserv/getusertasks", "POST",getData,"",listofleaves_callback,"",contentType);

  	/*var trail = getTemplate("leaveForms");
  	var list = [{"status":"opened","username":"ravi","typeofleave":"casual","purpose":"leave","startdate":"1/2/14","enddate":"2/2/14","leavedescription":"hi dude!!!"},{"status":"pending","username":"ravi","typeofleave":"casual","purpose":"leave","startdate":"1/2/14","enddate":"2/2/14","leavedescription":"hi dude!!!"},{"status":"opened","username":"ravi","typeofleave":"casual","purpose":"leave","startdate":"1/2/14","enddate":"2/2/14","leavedescription":"hi dude!!!"}];
  	var htmls = trail(list);
  	$('#Leaves').html(htmls); */

}

function listofleaves_callback(callBackArguments, result, status, xhr){

	$("#appliedLeaves").hide();
	$("Leaves").show();
    console.log(result);
    var obj = JSON.parse(result);
    if(obj.length==0){
    	console.log("no leaves");
    	$("#Leaves").html(
		"<div class='scrib_container no_res' style='padding-left:225px;'> Oops!! Nothing here </div>");
  
    }
    else{
    console.log(obj);
    console.log(obj[0]["appdata"]);
    leaveappid=obj[0]["appid"];
    var trail = getTemplate("leaveForms");
  	var htmls = trail(obj);
  	$('#Leaves').html(htmls);
  	}
}



function loadRecruitmentform(){
	$("#dyna_dashboard").hide();
	$("#dyna_mainContent").hide();
	$("#dyna_leaveform").hide();
	$("#dyna_recruitmentform").show();
	$("#dyna_expensereport").hide();
  	var trail1 = getTemplate("recruitmentForms");
  	var list1 = [{"status":"opened","username_recruitment":"ravi","skillset":"casual","experience":"leave","startdate":"1/2/14","contactnumber":"2/2/14","leavedescription":"hi dude!!!","username":"haaad"},{"status":"opened","username_recruitment":"ravi","skillset":"casual","experience":"leave","startdate":"1/2/14","contactnumber":"2/2/14","leavedescription":"hi dude!!!","username":"haaad"},{"status":"opened","username_recruitment":"ravi","skillset":"casual","experience":"leave","startdate":"1/2/14","contactnumber":"2/2/14","leavedescription":"hi dude!!!","username":"haaad"}];
  	var htmls1 = trail1(list1);
  	$('#Recruitments').html(htmls1); 

}

/*
=========> loading applied recruitment forms
*/
/*
function loadMyrecruitments(){
	$("#dyna_mainContent").hide();
	$("#dyna_leaveform").hide();
	$("#dyna_recruitmentform").show();
	$("#dyna_expensereport").hide();
	$("#dyna_dashboard").hide();

	var temp={};
	temp["userid"] = userData["id"];
	temp["appid"] = leaveappid;
  	var getData = {"jsondata" : JSON.stringify(temp)};
	contentType = "application/x-www-form-urlencoded",
	performAJAX( "wfserv/getmyrequests", "POST",getData,"",listofappliedrecruitments_callback,"",contentType);


}

function listofappliedrecruitments_callback(callBackArguments, result, status, xhr){

    console.log(result);
    var obj = JSON.parse(result);
    console.log(obj);
    console.log(obj[0]["appdata"]);
    leaveappid=obj[0]["appid"];
    var trail = getTemplate("appliedleaveForms");
  	//var list = [{"status":"opened","username":"ravi","typeofleave":"casual","purpose":"leave","startdate":"1/2/14","enddate":"2/2/14","leavedescription":"hi dude!!!"},{"status":"pending","username":"ravi","typeofleave":"casual","purpose":"leave","startdate":"1/2/14","enddate":"2/2/14","leavedescription":"hi dude!!!"},{"status":"opened","username":"ravi","typeofleave":"casual","purpose":"leave","startdate":"1/2/14","enddate":"2/2/14","leavedescription":"hi dude!!!"}];
  	var htmls = trail(obj);
  	$('#appliedLeaves').html(htmls); 
}
*/


function loadExpensereport(){
	$("#dyna_dashboard").hide();
	$("#dyna_mainContent").hide();
	$("#dyna_leaveform").hide();
	$("#dyna_recruitmentform").hide();
	$("#dyna_expensereport").show();
  	var trail2 = getTemplate("expenseReports");
  	var list2 = [{"username":"Calvin","expense_description":"casual","amount":"leave","date":"1/2/14","receipt_id":"2/2/14","leave_description":"hi dude!!!"},{"username":"Madhu","expense_description":"casual","amount":"leave","date":"1/2/14","receipt_id":"2/2/14","leave_description":"hi dude!!!"},{"username":"Marakkannu","expense_description":"casual","amount":"leave","date":"1/2/14","receipt_id":"2/2/14","leave_description":"hi dude!!!"}];
  	var htmls2 = trail2(list2);
  	$('#Expenses').html(htmls2); 
}


/*
=========> loading applied expense reports
*/
/*
function loadMyrecruitments(){
	$("#dyna_mainContent").hide();
	$("#dyna_leaveform").hide();
	$("#dyna_recruitmentform").hide();
	$("#dyna_expensereport").show();
	$("#dyna_dashboard").hide();

	var temp={};
	temp["userid"] = userData["id"];
	temp["appid"] = leaveappid;
  	var getData = {"jsondata" : JSON.stringify(temp)};
	contentType = "application/x-www-form-urlencoded",
	performAJAX( "wfserv/getmyrequests", "POST",getData,"",listofappliedrecruitments_callback,"",contentType);


}

function listofappliedrecruitments_callback(callBackArguments, result, status, xhr){

    console.log(result);
    var obj = JSON.parse(result);
    console.log(obj);
    console.log(obj[0]["appdata"]);
    leaveappid=obj[0]["appid"];
    var trail = getTemplate("appliedleaveForms");
  	//var list = [{"status":"opened","username":"ravi","typeofleave":"casual","purpose":"leave","startdate":"1/2/14","enddate":"2/2/14","leavedescription":"hi dude!!!"},{"status":"pending","username":"ravi","typeofleave":"casual","purpose":"leave","startdate":"1/2/14","enddate":"2/2/14","leavedescription":"hi dude!!!"},{"status":"opened","username":"ravi","typeofleave":"casual","purpose":"leave","startdate":"1/2/14","enddate":"2/2/14","leavedescription":"hi dude!!!"}];
  	var htmls = trail(obj);
  	$('#appliedLeaves').html(htmls); 
}
*/





/**
 * Loads DashBoard of an activity
 */
//var fromdashboard;

function loadDashboard() {
	$("#dyna_mainContent").hide();
	$("#dyna_dashboard").show();
	$("#dyna_leaveform").hide();
	$("#dyna_recruitmentform").hide();
	$("#dyna_expensereport").hide();

	$("#scrib-dash-set").html("");
	var activityId = getSelectedActivityId();
	$("#dyna_activity_title").html(activityId);
	//$("#dyna_activity_title").html(getActivityName(activityId));
	$("#dyna_activity_desc").html(getActivityDescription(activityId));

/*
====> performAJAX for saved workflows
*/
	var temp = {};
    temp["companyid"]="2";
    var getData = {"jsondata" : JSON.stringify(temp)};
    contentType = "application/x-www-form-urlencoded",


	performAJAX( "wfserv/getwkfs", "POST",getData,"",savedWorkflows_callback,"",contentType);
	

	performAJAX(getUserFileCountUrl(getUserId(), getUserOrgId(), activityId), "GET", "", "",
			loadTileSummary, "", "application/x-www-form-urlencoded");
	
	$("#dyna_activity_tasksSummary").empty(); 
	performAJAX(getTaskStatusURL(activityId), "GET", "", "",
			loadTaskStatus_callBack, "", "application/x-www-form-urlencoded");
	var activityDetails = getActivityById(activity,activitiesData);
	activitycreator=activityDetails.createdby;
	if(getUserId()==activitycreator){
	var template=getTemplate("pageHeader_workspacesettings");
	$("#scrib-dash-set").html(template);
	}
	else{
	$(".scrib-dash-set").hide();
	}
	
}


/*
=====> workflow call back function

*/

function savedWorkflows_callback(callBackArguments, result, status, xhr){

	var obj = JSON.parse(result);
	console.log(obj);
	var template = getTemplate("savedWorkflowstemplate");
	var html = template(obj);
	$("#workflowsSaved").html(html);
	
}


/**
====> ajax for getting apps
**/
function getapps(){

    contentType = "application/x-www-form-urlencoded",
	performAJAX( "wfserv/getapps", "POST","","",listofapps_callback,"",contentType);


}

function listofapps_callback(callBackArguments, result, status, xhr){

	var obj = JSON.parse(result);
	console.log(obj);
	var template = getTemplate("savedAppstemplate");
	var html = template(obj);
	$("#listofapps").html(html);
	var c = document.getElementById("chooser");
  	c.innerHTML="Step 1. Choosing App / Form"+'<button style="float:right;padding:3px;"type="button" onclick="returntonone()"class="btn btn-warning">Close</button>';

  	
}


/**
 * Fetches file count and calls for other details
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */
function loadTileSummary(callBackArguments, result, status, xhr) {
	var callBackArguments = {
		"fileCount" : "0","activityname":getSelectedActivityId()
	};	
	if (isSuccess(status)) {
		callBackArguments.fileCount = result.fileCount;
	}
	performAJAX(getActivitySummaryURL(getSelectedActivityId()), "GET", "", "",
			loadTileSummary_callBack, callBackArguments, "application/x-www-form-urlencoded");	
}

/**
 * Call-back method for loadDashboard loadTileSummary
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */
function loadTileSummary_callBack(callBackArguments, result, status, xhr) {
	if (isSuccess(status)) {
		var jsonData = [
			{"type":"Posts", "className":"creat-new-post glyphicon glyphicon-pencil scrib_dashboard_icn", "totalCount":result.post.totalcount,"activityname":callBackArguments.activityname},
			{"type":"Tasks", "className":"creat-new-task fa fa-check scrib_dashboard_icn", "totalCount":result.task.totalcount, "open":(result.task.open+result.task.inpro),"activityname":callBackArguments.activityname},
			{"type":"Queries", "className":"creat-new-query fa fa-question-circle scrib_dashboard_icn", "totalCount":result.query.totalcount, "open":result.query.open,"activityname":callBackArguments.activityname},
			{"type":"Members", "className":"flaticon-user4 scrib_users_bg scrib_dashboard_icn mem-height", "totalCount":activityMembers.length,"activityname":callBackArguments.activityname},
			{"type":"Files", "className":"creat-new-drive glyphicon glyphicon-folder-open scrib_dashboard_icn", "totalCount":callBackArguments.fileCount,"activityname":callBackArguments.activityname}
		];
		var template = getTemplate("dashboardTileSummaryTemplate");
		var html = template(jsonData);
		$("#dyna_activity_tilesSummary").html(html);
	}
	$("#"+jsonData[1].type+"_"+callBackArguments.activityname).click(function(){
	if(tiletype=='dashboard'){
		tileType="task";
		tiletype=tileType;
		filterArray=new FilterController().getFilters(activity,tiletype);
		getopenedtiles(tileType);
		}
		});
		$("#"+jsonData[2].type+"_"+callBackArguments.activityname).click(function(){
	if(tiletype=='dashboard'){
		tileType="query";
		tiletype=tileType;
		filterArray=new FilterController().getFilters(activity,tiletype);
		getopenedtiles(tileType);
		}
		});
}

/**
 * Call-back method for loadDashboard loadTaskStatus
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */
function loadTaskStatus_callBack(callBackArguments, result, status, xhr) {
	$("#dyna_activity_tasksSummary").empty(); 
	if (isSuccess(status)) {
		var chartInputData = [];
		var chartHeader = ['User', 'Completed', { role: 'annotation' }, 'In Progress', { role: 'annotation' }, 
		                   'Due to start', { role: 'annotation' }];
		chartInputData.push(chartHeader);
		if(result.users.length != 0) {
			for(i=0;i<result.users.length;i++) {
				var totalCountForAUser = result.stat[2][i] + result.stat[1][i] + result.stat[0][i];
				if(totalCountForAUser > 0) {
					var chartData = [(showUser(result.users[i])), result.stat[2][i], result.stat[2][i], result.stat[1][i],result.stat[1][i],
					                 result.stat[0][i], result.stat[0][i]];
					chartInputData.push(chartData);
				}
			}
		
			var chartDivObj = null;
			var arr = $("div[class='dyna_activity_tasksSummary']");
			$.each( arr, function(index, value) {
				chartDivObj = value;
				generateBarChart(chartDivObj, chartInputData);
			});
		}
	}
}

/*** 
 * @param activityId
 * toload activity specific tiles
 */
function loadSelectedActivityTiles(activityId) {
	  $('.leftbar').removeClass('show-nav');
	
	$("#ActivitySettings_dashboard").hide();
	$(window).unbind('scroll');
	$(document).prop('title', "ScribLeaf");
	 showCommonHeader();
     loadTracker("update");
	tiletype="All";
	updatesCount=0;
	time=null;
	 $("#info_block").attr("style","display:none");
	 makeTileTypeActive(activity,'All');
	makeActivityActive(activityId);
	$("#dyna_mainContent").show();
	$("#dyna_dashboard").hide();
	starred=false;	
	isFilteredTiles=false;
	setSelectedActivityId(activityId);
	loadActivityMembers(activityId);
	$(".ac-"+activityId).prop("checked","true");
	removeLink("All");
	addLink("Assigned","getAssignedTiles()");
	addLink("BookMarked","loadStarredTiles()");
	$("#activitiesOfOrganization").find("li").remove();
	tileData = [];	
	$("#showTilesOrNotifications").find(".scroles").remove();
	$(".scrib_slidedown").hide();
	loadTiles(view, activityId, 'All');
	showTilesCount();
	$("#ac-2").prop("checked","true");
	$("#lg-2").prop("checked","true");
	makeOutboxLabelActive('2');
}

/*******************************************************************************
 * to initiate pagination
 * @param viewType
 * @param activityId
 * @param tileType
 */
function initiatePaginationOnScroll(viewType, activityId, tileType) {

	 $(window).scroll(function(){
         if  ($(window).scrollTop() >= ($(document).height() - $(window).height())*0.8){
        	 loadTiles(viewType, activityId, tileType);
        	 $(window).unbind(
				'scroll');
         }
 });
}

/*******************************************************************************
 * @param fileInputId
 * to browse files
 */
function browseFiles(fileInputId) {
	$(fileInputId).click();
}

/*******************************************************************************
 * @param files
 * @param statusDivId
 * @param tileType
 * @this function is used to upload files on both TILE and LIGHTBOX
 */
function handleFiles(files, statusDivId, tileType) {
	if(isFileAttached){
		$('#'+activity+"_Post").click();
		isFileAttached=false;
		goToDrive=true;

	}
	handleUploadFiles(files, statusDivId, tileType);
}

function handleFilesOnLightBox(files, statusDivId, tileType) {
	handleUploadFiles(files, statusDivId, tileType);
}

/**
 * to add attachments 
 * 
 * @param files
 * @param statusDivId
 * @param tileType
 */
function handleUploadFiles(files,statusDivId,tileType){
if (files.length) {
		for (i = 0; i < files.length; i++) {
			$(".ContFiles").fadeIn();
			var fd = new FormData();
			 if(isNewFile(files[i])){
			fd.append('file', files[i]);
			uploadAttachment(fd,statusDivId,tileType);

		}
			 }
	}
}
/*******************************************************************************
*@param filepath
*to check is new file or not
*/
 function isNewFile(file){
   var response=true;
  
   if(attachedIds.length>0) {
   var filename = file.name;
  
     for(var i=0 ; i <attachedIds.length; i++){
       var attachmentName=attachedIds[i];
       var checkName=attachmentName.substring(attachmentName.lastIndexOf("/")+1,attachmentName.length);
       if(filename==checkName){
         response=false;
         break;
       }
       else{
        response=true;
       }
     }
   }else {
     response=true;
     
  }
  return response;
 }

/*******************************************************************************
 * @param tileType
 * to create particular tiletype ui
 */
function create(tileType) {
	resetCreateUI(tileType);
	bindEventsOnCreateTile(tileType);
	setStartDateAndDueDate(tileType,"#dyna_create_" + tileType + "_start","#dyna_create_" + tileType + "_due")
	
}

/**
 * to bind datepicker on tiletypes
 * @param tileType
 * @param startDateId
 * @param dueDateId
 */

function setStartDateAndDueDate(tileType,startDateId,dueDateId){	

	if (tileType == 'task') {
		$(startDateId)
				.datepicker(
						{    
							minDate : new Date(),
							onClose : function(selectedDate) {
								$(dueDateId)
										.datepicker("option", "minDate",
												selectedDate);
								if(selectedDate!="Today"&&selectedDate!=""){
								var temp = selectedDate.split("/");
								var currentDate = new Date(new Number(temp[2]),
										(new Number(temp[0]) - 1), new Number(
												temp[1]));
								}else{
									var currentDate=new Date();
								}
								var ms = currentDate.getTime();
								if(($(dueDateId).val()<currentDate)||($(dueDateId).val()=="Tomorrow")){
								$(dueDateId).val(
										$.format.date(new Date(ms),
												DEFAULT_DATE_FORMAT));
							}
							}
						});
		$(dueDateId).datepicker(
				{
					minDate : new Date(),
					onClose : function(selectedDate) {
						if(selectedDate!="Tomorrow"&&selectedDate!=""){
						$(startDateId).datepicker(
								"option", "maxDate", selectedDate);
					}
						else{
							$(startDateId).datepicker(
									"option", "maxDate", selectedDate);
						}
						
						var startDate=$(startDateId).val();
						if(startDate==""||startDate==undefined||startDate=="Today"){
							$(startDateId).val($.format.date(new Date(),
									DEFAULT_DATE_FORMAT));
						}
						
					}
				});
	} else if (tileType == 'query') {
		
		var maxDateString = $.format.date(new Date(), DEFAULT_DATE_FORMAT);
		var tempmaxDate = new Date(maxDateString);
		tempmaxDate.setDate(tempmaxDate.getDate() + 14);
		var maxdate = tempmaxDate;
		
		$(dueDateId).datepicker(
				{   
					minDate : new Date,
					maxDate:maxdate,
					onClose : function(selectedDate) {
						if (selectedDate != "Tomorrow") {
							var temp = selectedDate.split("/");
							var currentDate = new Date(new Number(temp[2]),
									(new Number(temp[0]) - 1), new Number(
											temp[1]));
							var ms = (currentDate).getTime();

							$(dueDateId).val(
									$.format.date(new Date(ms),
											DEFAULT_DATE_FORMAT));
						}
					}
				});
	} else {
		
	}
	}
/*******************************************************************************
 * @param tileType
 * to bind events on create tiletypes
 */
function bindEventsOnCreateTile(tileType) {
	$(".createJourney_" + tileType).on('shown.bs.modal', function() {
		if(!isIe){
		$("#dyna_create_" + tileType + "_title").focus();
	}
		  $("#dyna_create_"+tileType+"_description").autosize().show().trigger('autosize.resize');
		  showplc();

	});
	
	$(".AddDet").show();	
	 $('[rel="tooltip"]').tooltip();
	

	$('.cjText').keyup(function() {
		$('.cjRightLink').fadeIn("slow");
	});

	$('.cjDetails').keyup(function() {
		$('.cjRightLink2').fadeIn("slow");
	});

	
	
	$('.scrib-toggle-down').hide();
	$( ".scrib-title-hover" ).mouseenter(function() {
	  $('.scrib-toggle-down').show();
	});

	$('.scrib-toggle-down').click(function(){
	$(this).hide();
	});

	$('.scrib-toggle-down').mouseleave(function(){                         
	$(this).hide();                           
	});
	
	

	

	$(".image1").click(function(event) {
		$(this).attr("style","display:none");
		$(".image2").attr("style","display:block");
	});

	$(".image2").click(function(event) {
		$(this).attr("style","display:none");
		$(".image1").attr("style","display:block");
	});
	
	$(".image1").keydown(function(e){
		if(e.keyCode==13){
			$(".image1").click();
		}
	});
	
	$(".image2").keydown(function(e){
		if(e.keyCode==13){
			$(".image2").click();
		}
	});
	
	$(".AddLabels").css("display", "none");
	$(".AddLabelLink").click(function() {
		$(".AddLabels").fadeIn();
		if(!isIe){
			$(".widget4").focus();
		}
		
	});
	$(".AddLabelTabIndex").keydown(function(e){
		if(e.keyCode == 13){
			$(".AddLabelLink").click();
		}
	});
	$(".ContFiles").css("display", "none");
	$(".AddFoolowersBox").css("display", "none");
	$(".AddFollowersLink").click(function() {
		$(".AddFoolowersBox").fadeIn();
	});

	$(".AddDetails").css("display", "none");
	$(".AddDet").click(function() {
		$(".AddDetails").fadeIn();
		if(!isIe){
			$("#dyna_create_" + tileType + "_description").focus();
		}
		$("#dyna_create_" + tileType + "_description").focus();
		$("#dyna_create_"+tileType+"_description").attr("style","min-height:50px");
		$("#dyna_create_" + tileType + "_description").autosize();
		$(".AddDet").hide();			
	});
	
	$(".AddDet").keydown(function(e) {
		 if (e.keyCode == 13) {
			 $(".AddDet").click();
		  }
	});

	$('.modal-dialog').keydown(function (e) {
			  if (e.ctrlKey && e.keyCode == 13) {
				 $("#dyna_create_"+tileType+"_submit").click();
			  }
			  if(e.keyCode == 27){
				  $(".createJourney_" +tileType).modal('hide');
				  closeCreateTile(tileType);
			  }
			});
	$("#dyna_create_" + tileType + "_title").keydown(function (e) {
		 if (!e.ctrlKey && e.keyCode != 13){
			 $("#dyna_create_" + tileType + "_title").removeClass("red");
			
			 removePlaceholderClass("#dyna_create_" + tileType + "_title");

	var title=$("#dyna_create_" + tileType + "_title").val();

	if(title.length>=1700){
		showWarning(tileType);
	}
		 }
		 });
	
	$("#dyna_create_" + tileType + "_assignee").keydown(function (e) {
		 if (!e.ctrlKey && e.keyCode != 13){
				$("#dyna_create_" + tileType + "_assignee").removeClass("red");
				removePlaceholderClass("#dyna_create_" + tileType + "_assignee");
	
		 }
	});
	
	$("#dyna_create_"+tileType+"_description").keypress(function(e){
		var details=$("#dyna_create_"+tileType+"_description").val();
	
		if(details.length>=1700){
			showWarning(tileType);
		}
	});
	

}

/**
 * to show warnings 
 * @param tileType
 */


function showWarning(tileType){
	  $("#alert_block_"+tileType).alert();
      $("#alert_block_"+tileType).fadeTo(10000, 500).fadeOut(500, function(){
      });
 	 $(".close").click(function(){
		 $("#alert_block_"+tileType).hide(); 
	});
}

/**
 * to reset create ui
 * @param tileType
 */

function resetCreateUI(tileType) {
    var titlePlaceholder="";
    var assigneePlaceholder="";
    if(tileType=='post'){
    	titlePlaceholder="Update here";	
    	assigneePlaceholder="Notify members";
    }
    if(tileType=='task'){
    	titlePlaceholder="Write your Task here";	
    	assigneePlaceholder	="Add Assignees";
     }
    if(tileType=='query'){
    	titlePlaceholder="Write your query here";		
    	assigneePlaceholder="Notify specific people"; 
    }
    if(tileType=='recruitmentform'){
    	titlePlaceholder="Name of Applicant";
    	skillSet="Skill - Set of Applicant";
    	experience="Number of Years of Experience";
    	contactnumber="Contact Number";
    	stagename="Enter Stage Name";
    	approvername="Enter Approver Name";
    }
    if(tileType=='leaveform'){
    	var te = document.getElementById("leave");
    	te.style.display="block";
    	titlePlaceholder="Type of leave";
    	purposeholder = "Purpose of leave";
    	startdateholder="Select date";
    	enddateholder="Select date";
    	descrip="Description";

    }
    if(tileType=='expensereport'){
    	dateHolder = "Select Date";
    	reasonHolder = "Description";
    	amountHolder = "Amount in Rs .";
    	receiptHolder = "Receipt Id";
    }

    $(".image1").hide();
    $(".image2").attr("style","display:block");

	$("#dyna_create_" + tileType + "_title").get(0).value = "";
	$("#dyna_create_" + tileType + "_title").removeClass("red");

		removePlaceholderClass("#dyna_create_" + tileType + "_title");

	$("#dyna_create_" + tileType + "_title").attr("placeholder",titlePlaceholder);



/**
==> start of reseting expense report placeholders
**/
	if(tileType=="expensereport")
	{
		$("#dyna_create_" + tileType + "_expensedate").get(0).value = "";
		$("#dyna_create_" + tileType + "_reason").get(0).value = "";
		$("#dyna_create_" + tileType + "_amount").get(0).value = "";
		$("#dyna_create_" + tileType + "_receipt").get(0).value = "";

		$("#dyna_create_" + tileType + "_expensedate").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_expensedate");

		$("#dyna_create_" + tileType + "_reason").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_reason");
		
		$("#dyna_create_" + tileType + "_amount").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_amount");
		
		$("#dyna_create_" + tileType + "_receipt").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_receipt");


		$("#dyna_create_" + tileType + "_expensedate").attr("placeholder",dateHolder);
		$("#dyna_create_" + tileType + "_reason").attr("placeholder",reasonHolder);
		$("#dyna_create_" + tileType + "_amount").attr("placeholder",amountHolder);
		$("#dyna_create_" + tileType + "_receipt").attr("placeholder",receiptHolder);

		$("#dyna_create_" + tileType + "_selectedActivity").html(
			getSelectedActivityNameForLightBox(getSelectedActivityId()));

		workspacename = getSelectedActivityId();
	var activityTemplateLightBox = getTemplate("activityTemplateLightBox");
	var html = activityTemplateLightBox(getActivitiesDataForLightBox(tileType));
	$("#dyna_create_" + tileType + "_activities").html(html);
	
	}



/**
===> start of reseting recruitment form placeholders
**/
	if(tileType=="recruitmentform"){
	$("#dyna_create_" + tileType + "_skill").get(0).value = "";
	$("#dyna_create_" + tileType + "_experience").get(0).value = "";
	$("#dyna_create_" + tileType + "_contactnumber").get(0).value = "";
	$("#dyna_create_" + tileType + "_stage").get(0).value = "";
	$("#dyna_create_" + tileType + "_approver").get(0).value = "";

		$("#dyna_create_" + tileType + "_skill").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_skill");

		$("#dyna_create_" + tileType + "_experience").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_experience");
		
		$("#dyna_create_" + tileType + "_contactnumber").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_contactnumber");
		
		$("#dyna_create_" + tileType + "_stage").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_stage");

		$("#dyna_create_" + tileType + "_approver").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_approver");

	$("#dyna_create_" + tileType + "_skill").attr("placeholder",skillSet);
	$("#dyna_create_" + tileType + "_experience").attr("placeholder",experience);
	$("#dyna_create_" + tileType + "_contactnumber").attr("placeholder",contactnumber);
	$("#dyna_create_" + tileType + "_stage").attr("placeholder",stagename);
	$("#dyna_create_" + tileType + "_approver").attr("placeholder",approvername);
	
		$("#dyna_create_" + tileType + "_selectedActivity").html(
			getSelectedActivityNameForLightBox(getSelectedActivityId()));

		workspacename = getSelectedActivityId();
	var activityTemplateLightBox = getTemplate("activityTemplateLightBox");
	var html = activityTemplateLightBox(getActivitiesDataForLightBox(tileType));
	$("#dyna_create_" + tileType + "_activities").html(html);
	}
/**
===> end
**/

/*
===> reseting leave form placeholders
*/
	if(tileType=="leaveform"){
	$("#dyna_create_" + tileType + "_purpose").get(0).value = "";
	$("#dyna_create_" + tileType + "_startleave").get(0).value = "";
	$("#dyna_create_" + tileType + "_dueleave").get(0).value = "";
	$("#dyna_create_" + tileType + "_descrip").get(0).value = "";

		$("#dyna_create_" + tileType + "_purpose").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_purpose");
		
		$("#dyna_create_" + tileType + "_startleave").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_startdate");

		$("#dyna_create_" + tileType + "_dueleave").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_dueleave");

		$("#dyna_create_" + tileType + "_descrip").removeClass("red");
		removePlaceholderClass("#dyna_create_" + tileType + "_descrip");

	
	$("#dyna_create_" + tileType + "_purpose").attr("placeholder",purposeholder);
	$("#dyna_create_" + tileType + "_startleave").attr("placeholder",startdateholder);
	$("#dyna_create_" + tileType + "_dueleave").attr("placeholder",enddateholder);
	$("#dyna_create_" + tileType + "_descrip").attr("placeholder",descrip);
	
		//$("#dyna_create_" + tileType + "_selectedActivity").html(
		//	getSelectedActivityNameForLightBox(getSelectedActivityId()));

		$("#dyna_create_" + tileType + "_selectedActivity").html(
				getSelectedActivityId());

			workspacename = getSelectedActivityId();
	var activityTemplateLightBox = getTemplate("activityTemplateLightBox");
	var html = activityTemplateLightBox(getActivitiesDataForLightBox(tileType));
	$("#dyna_create_" + tileType + "_activities").html(html);
	}
/*
===> END of reseting placeholders for leave form
*/
	
	$("#dyna_create_" + tileType + "_description").get(0).value = "";
	$("#dyna_create_" + tileType + "_label").get(0).value="";
	$(".rightLabels2").hide();
	$("#dyna_create_" + tileType + "_files").find("span").remove();
	$("#dyna_create_" + tileType + "_selectedActivity").html(
			getSelectedActivityNameForLightBox(getSelectedActivityId()));
	var activityTemplateLightBox = getTemplate("activityTemplateLightBox");
	var html = activityTemplateLightBox(getActivitiesDataForLightBox(tileType));
	$("#dyna_create_" + tileType + "_activities").html(html);
	

	
	
	
	$("#dyna_create_"+tileType+"_assignee").get(0).value = "";
	$("#dyna_create_"+tileType+"_assignee").removeClass("red");

		removePlaceholderClass("#dyna_create_"+tileType+"_assignee");

	$("#dyna_create_"+tileType+"_assignee").attr("placeholder",assigneePlaceholder)
	$("#dyna_create_" + tileType + "_assignees").find(".CJdocuments").remove();
	searchUser("#dyna_create_" + tileType + "_assignee", "#dyna_create_"
			+ tileType + "_assignees_list", tileType,activity,false);
	
	informEmailUser("#dyna_create_" + tileType + "_assignee");
	
	$("#dyna_create_" + tileType + "_labels").find(".CJdocuments").remove();
	searchLabels("#dyna_create_" + tileType + "_label", "#dyna_create_"
			+ tileType + "_labels_list", tileType,activity);
	addLabel("#dyna_create_" + tileType + "_label", tileType);
	
	$(".PopUptooltip").stop().fadeOut('fast');
	
	if (tileType == 'task') {
		$("#dyna_create_" + tileType + "_start").get(0).value = "Today";
		$("#dyna_create_" + tileType + "_due").get(0).value = "Tomorrow";
		$("#dyna_create_" + tileType + "_start").datepicker("destroy");
		$("#dyna_create_" + tileType + "_due").datepicker("destroy");
	} else if (tileType == 'query') {
		$("#dyna_create_" + tileType + "_due").datepicker("destroy");
		var targetDateString = $.format.date(new Date(), DEFAULT_DATE_FORMAT);
		var tempDate = new Date(targetDateString);
		tempDate.setDate(tempDate.getDate() + 3);
		var targetDate = tempDate;
		$("#dyna_create_" + tileType + "_due").val(
				$.format.date(targetDate, DEFAULT_DATE_FORMAT));
	}

	


}

/**
 * to remove text field value
 * @param sourceID
 */
function informEmailUser(sourceID){
	$(sourceID)
	.keydown(
			function(e) {
				if (e.keyCode == 13) {
					$(sourceID).val("");
				}});
	
}



/**
 * to displays activities on create journey
 * @param tileType
 * @returns {Array}
 */
function getActivitiesDataForLightBox(tileType) {
	var tempActivities = new Array();
	$.each(activitiesData, function(i, activity) {
		var newActivity = new Object();
		newActivity.id = activity.id;
		if (activity.id == HOME_ID)
			newActivity.name = "PUBLIC";
		else
			newActivity.name = activity.name;

		newActivity.tileType = tileType;
		if (activity.id == getSelectedActivityId())
			newActivity.selectedActivity = true;
		else
			newActivity.selectedActivity = false;
		tempActivities.push(newActivity);
	});
	return tempActivities;
}

/*******************************************************************************
 * to set selected activity id
 * @param id
 * @param tileType
 */
function selectActivityFromLighBox(id, tileType) {
	showplc();
	$("#dyna_create_" + tileType + "_assignee").autocomplete( "close" );
	$("#dyna_create_" + tileType + "_label").autocomplete( "close" );	
	searchUser("#dyna_create_" + tileType + "_assignee", "#dyna_create_"
			+ tileType + "_assignees_list", tileType,id,false);
	
	searchLabels("#dyna_create_" + tileType + "_label", "#dyna_create_"
			+ tileType + "_labels_list", tileType,id);
	
	selectedToList = [];
	selectedTagsList=[];
	
	
	$("#dyna_create_" + tileType + "_labels").find(".CJdocuments").remove();
	$("#dyna_create_" + tileType + "_assignees").find(".CJdocuments").remove();
	$("#dyna_create_" + tileType + "_activities").find("li").find("a").attr("style","background-color:");
	$("#dyna_create_" + tileType + "_activities").find("li").find("a").find("i").remove();
	$("#li_activity_" + id).find("a").attr("style","background-color:#DAE9F7");
	$("#li_activity_" + id).find("a").append("<i id="+id+"></i>");
	$("#dyna_create_" + tileType + "_selectedActivity").html(
			getSelectedActivityNameForLightBox(id));
	$(".PopUptooltip").stop().fadeOut('fast');
}

/**
 * to get selected activity name on create journey
 * @param id
 * @returns {String}
 */
function getSelectedActivityNameForLightBox(id) {
	var selectedActivityName = "";
	if (id == HOME_ID){
		selectedActivityName ="\"PUBLIC\"";
	}
	else
		$.each(activitiesData, function(i, activity) {
			if (id == activity.id)
				selectedActivityName ="\""+ activity.name+"\"";
		});
	return selectedActivityName;
}

/**
 * to return selected users 
 * @param data
 * @returns
 */
var nonDuplicatesCount;
function showToIds(data) {
	var showUserTemplate = getTemplate("showUserTemplate");
	var nonDuplicates = new Array();
	nonDuplicatesCount = 0;
	for (i = 0; i < data.length; i++) {
		var count = 0;
		for (j = i + 1; j < data.length; j++) {
			if (data[i].id == data[j].id) {
				count++;
			}
		}
		if (count == 0) {
			nonDuplicates[nonDuplicatesCount] = data[i];
			nonDuplicatesCount++;
		}
	}
	var htmlContent = showUserTemplate(nonDuplicates);
	return htmlContent;
}

/**
 * 
 * to close the crete tile
 * @param tileType
 */
function closeCreateTile(tileType){
	goToDrive=false;
	resetCreateUI(tileType);
	makeCreateListEmpty()
	$("#smartlisttileType").val("");
	$(".scrib_cmt_scrl").scrollTop(0);
	$(".createJourney_" + tileType).modal('hide');
	if(tileTobeCreated == false){
		
		var gridList = getGridObject(); 
		gridList.render();
		$('#'+getListId()).dialogExtend('restore');
		var ele = $("#conversation_list").find("#smartListMinimizeDiv");
		$(ele).remove();
	}
}
/**
 * to reset all arrays
 */
function makeCreateListEmpty(){
	selectedToList = [];
	attachedIds = [];
    selectedTagsList=[];
	openLBCount=0;
	 openLBtitlearray=[];
}

/**
 * to show selected tags 
 * @param data
 * @returns
 */
function showTagsList(data){
var showTagsTemplate = getTemplate("showTagsTemplate");
var nonDuplicates = new Array();
var nonDuplicatesCount = 0;
for (i = 0; i < data.length; i++) {
	var count = 0;
	for (j = i + 1; j < data.length; j++) {
		if (data[i].tag == data[j].tag) {
			count++;
		}
	}
	if (count == 0) {
		nonDuplicates[nonDuplicatesCount] = data[i];
		nonDuplicatesCount++;
	}
}
var htmlContent = showTagsTemplate(nonDuplicates);
return htmlContent;
}

/*******************************************************************************
 * auto suggestion for users
 * @param sourceId
 * @param targetId
 */
function searchUser(sourceId, targetId, tileType,activityId,userSelector) {


	$(targetId).show();
	$(sourceId).autocomplete(
			{
				minLength : 2,
				selectFirst: true,
				autoFocus:true,
				messages : {
					noResults : '',
					results : function() {
					}
				},
				source : function(request, response) {
					var url=getUserSearchURL($(sourceId).val(),activityId);
					if(userSelector&&accountType=="Enterprise"){
						url=getUserSearchURLForOrg($(sourceId).val(),HOME_ID);
					}
					$.ajax({
						url : url,
						success : function(data) {
							var tempData = [];
							$.each(data, function(index, item) {
								$.each(item, function(indexIndex, itemItem) {
									tempData.push(itemItem);
								});
							});
							response($.each(tempData, function(index, item) {
								item.HOME_ID = HOME_ID;
								if (item.name == undefined) {
									item.name = item.fname + " " + item.lname
									item.isDL = "false";
									if (item.avatar == null) {
										item.noImageFound = "true";
									} else {
										item.noImageFound = "false";
									}
								} else {
									item.fname = item.name;
									item.email = "Distribution List"
									item.isDL = "true";
								}
								return item;
							}));

						}
					});
				},
				  focus: function( event, ui ) { event.preventDefault();
				  },
				  
				  select : function(event, ui) {
					if(userSelector) {
						return searchUserSelector(event, ui, $(this),sourceId, targetId, tileType);
					}
					else {
					
						return searchUserDefaultSelector(event, ui, $(this), sourceId, targetId, tileType,"Add Assignees");
					}
				}

			}).data("ui-autocomplete")._renderItem = function(ul, item) {
		var userSearchTemplate = getTemplate("userSearchTemplate");
		var htmlContent = userSearchTemplate(item);
		return $("<li>").append("<a>" + htmlContent + "</a>").appendTo(ul);
	};
  

}

/*******************************************************************************/
/** default selector function for search user. 
 * 
 */

var searchUserDefaultSelector = function(event, ui, thisElement, sourceId, targetId, tileType,placeholder) {
	ui.item.tileType = tileType;
	selectedToList.push(ui.item);
	var ele = thisElement.parent().parent().find(".cjDetails");
	thisElement.parent().parent().find(
	".CJdocuments").remove();
	$(showToIds(selectedToList)).insertBefore(ele)
	$(sourceId).val("");
	
	$(sourceId).attr(
		"placeholder", placeholder);
	return false;
}

/**
 * auto suggestion for labels
 * @param sourceId
 * @param targetId
 * @param tileType
 */


function searchLabels(sourceId, targetId, tileType,activityId) {

	$(targetId).show();
	$(sourceId).autocomplete(
			{
				minLength : 2,
				selectFirst: true,
				autoFocus:true,
				messages : {
					noResults : '',
					results : function() {
					}
				},
				source : function(request, response) {
					$.ajax({
						url : getLabelsURL($(sourceId).val(),activityId, getUserId()),
						success : function(data) {
							var tempData = [];
							var LabelObject=[];
							$.each(data, function(index, item) {
							var LabelObject={
									tag:item,
							        tileType:tileType
							}
							tempData.push(LabelObject);
								
							});
							response($.each(tempData, function(index, item) {
								return item;
							}));

						}
					});
				},
				focus: function( event, ui ) { event.preventDefault();
				  },
				select : function(event, ui) {
					selectedTagsList.push(ui.item);
					var ele = $(this).parent().parent()
							.find(".cjDetails");
					$(this).parent().parent().find(
							".CJdocuments").remove();
					$(showTagsList(selectedTagsList)).insertBefore(ele);
					$(sourceId).val("");
					if(sourceId=="#post_add_Categories")
							{
								$(this).attr(
							"placeholder", "Add Categories");
							}
					else if(sourceId=="#add_label"){
						$(this).attr(
								"placeholder", "Add file categories - Organise files into relevant categories.");
								}
					
						else{
							$(this).attr(
							"placeholder", "Add labels");
							}
					return false;
				}

			}).data("ui-autocomplete")._renderItem = function(ul, item) {
		var tagsSearchTemplate = getTemplate("tagsSearchTemplate");
		var htmlContent = tagsSearchTemplate(item);
		return $("<li>").append("<a>" + htmlContent + "</a>").appendTo(ul);
	};

}

/**
 * to create tile from smartlist or normal
 * @param tileType
 */
function createTile(tileType){
	clearValue();
	if(tileTobeCreated == true)
		{
		submit(tileType)
		}
	else{
		linkTileWithSmartSheet(tileType)
	}
}


/**
 * to get create journey values for tile creation
 * @param tileType
 * @returns {___anonymous70235_70519}
 */
function FormToJSON(form){
      var array = jQuery(form).serializeArray();
      var json = {};
      var temp = {};
      var arr = [];
      var flag = 0;
      var i = 0;
      jQuery.each(array, function() {
        if(this.name == 'stage'){
          temp[this.name] = this.value || '';
          flag = flag+1;
        }
        else if(this.name == 'approver'){
          temp[this.name] = this.value || '';
          flag = flag+1;
        }
        else{
          json[this.name] = this.value || '';
        }
        if(flag % 2 == 0 && flag !=0){
          arr.push(temp);  
          temp = {};
        }
      });
      
      json["definition"] = arr;
      json["createddate"]="";
      json["createdby"]="";
      json["companyid"]="1";
      
      return json;
    }

   function LeaveFormToJSON(form){
      var array = jQuery(form).serializeArray();
      var json = {};
      var temp = {};
      var arr = [];
      var flag = 0;
      var i = 0;
      jQuery.each(array, function() {
          json[this.name] = this.value || '';
      });
      	console.log(json);
     	delete json["types"];

      return json;
    } 

 /*function listofleaves_callback(callBackArguments, result, status, xhr){
	console.log(result);
}*/
function getCustomisedDataForTileCreation(tileType){
		
	
	if (validateBeforeSubmit(tileType)) {
		$("#dyna_create_"+tileType+"_submit").attr("onclick","");

		
/**				
===> Json creating for recruitment form
**/
/*
		if(tileType == "recruitmentform"){
			
				var tileDataTemplate = {};
			
				jQuery('form#new-recruitment-form').bind('submit', function(event){
        				event.preventDefault();
        				var form = this;
       					var json = FormToJSON(form);   
        				//console.log(json);
        				tileDataTemplate = json;
        				return tileDataTemplate;
     			 });
			return null;
			

		}
*/
		 if(tileType=="leaveform"){
						var tileDataTemplate = {};
						var arr = [];
						var trails = {};
						jQuery('form#new-leave-form').bind('submit', function(event){
						//console.log("entered");	
						var te = document.getElementById("leave");
						te.style.display="none";
        				event.preventDefault();
        				var form = this;
       					var json = LeaveFormToJSON(form);   
        				tileDataTemplate = json;
        				//console.log(json);
						var postData = {"appdata" :  tileDataTemplate };
						trails["procid"] = "1234";
						trails["workspaceid"] = workspacename;
						trails["userid"] = userData["id"];
						trails["appid"] = "app_1";
						arr.push(tileDataTemplate);
						trails["appdata"] = arr;
						trails["status"] = "pending";
						trails["date"] = "12-11-2014";
						console.log("trails");
						console.log(trails);
						var jsondata = {"jsondata" : JSON.stringify(trails) };
        				contentType = "application/x-www-form-urlencoded",
						performAJAX( "wfserv/staticflow", "POST",jsondata,"",listofleaves_callback,"",contentType);
     			 });
					
		}
		else{
		var tileDataTemplate = {
			id : null,
			title : "",
			teaser : null,
			tiletype : "",
			createdby : "",
			attachment : "",
			data : "",
			createddate : "",
			activityLinked : "",
			activityAssigned : "",
			activityid : "",
			recipients : {
				"users" : "",
				"dls" : ""
			},
		    tags:[]
		};

		var sepecificTileDataTemplate = {
			"attributes" : null,
			"attributeMaps" : null
		};

		var dueDateTemplate = {
			"dueDate" : ""
		};
		var startDateTemplate = {
			"startDate" : ""
		};

		var attributes = [];
		
		if ($("#dyna_create_" + tileType + "_description").val() != ""){
			tileDataTemplate.teaser = $(
					"#dyna_create_" + tileType + "_description").val().replace(
					/(\r\n|\n|\r)/gm, "<br>");
		

		
		}

		tileDataTemplate.title = $("#dyna_create_" + tileType + "_title").val().replace(
				/(\r\n|\n|\r)/gm, "<br>");
		
		
		
		tileDataTemplate.tiletype = tileType;
		tileDataTemplate.createdby = isValueNotEmpty(getUserId()) ? getUserId()
				: null;
		tileDataTemplate.createddate = $.format.date(new Date(),
				"yyyy-MM-dd HH:mm:ss");

		tileDataTemplate.activityid = $(
				"#dyna_create_" + tileType + "_activities").find("li")
				.find("a").find("i").attr('id');
		tileDataTemplate.activityLinked = "Y";

		var temp_Attachements = [];
		for (i = 0; i < attachedIds.length; i++) {
			temp_Attachements.push(attachedIds[i]);
		}
		tileDataTemplate.attachment = (temp_Attachements.length > 0) ? temp_Attachements
				: null;

		var userIds = [];
		var dlIds = [];
		$.each(selectedToList, function(index, item) {
			
                        if (item.isDL == "true") {               
				dlIds.push(item.id);
			} else {
				userIds.push(item.id);
			}
		});
		
		var tags=[];
		
		var inputvalue=$("#dyna_create_"+tileType+"_label").val();
		if(inputvalue!="")
			{
			var item = {
				    "tag" : inputvalue,
		   		    "tileType" : ""
		    			};
		selectedTagsList.push(item);
	}
		$.each(selectedTagsList, function(index, item) {
         tags[index]=item.tag;			

       });
		
		tileDataTemplate.tags=tags;
		tileDataTemplate.recipients.users = userIds;
		tileDataTemplate.recipients.dls = dlIds;

		if ($("#dyna_create_" + tileType + "_lock").is(":visible")) {
			tileDataTemplate.activityAssigned = "N";
		} else {
			tileDataTemplate.activityAssigned = "Y";
		}

		if (tileType == 'query' || tileType == 'task') {
			var duedateText = $("#dyna_create_" + tileType + "_due").get(0).value;

			if (duedateText == 'Tomorrow') {
				var ms = new Date().getTime() + (86400000 * 1);
				dueDateTemplate.dueDate = $.format.date(new Date(ms),
						"yyyy-MM-dd HH:mm:ss");
			} else {
				var temp = duedateText.split("/");
				var dueDate = new Date(new Number(temp[2]),
						(new Number(temp[0]) - 1), new Number(temp[1]));
				dueDateTemplate.dueDate = $.format.date(dueDate,
						"yyyy-MM-dd HH:mm:ss");
			}
			attributes.push(dueDateTemplate);

		}
if(tileType=='task') {
	var startDateText = $("#dyna_create_" + tileType + "_start").get(0).value;

	if (startDateText == 'Today'||startDateText == '') {
		var ms = new Date().getTime();
		startDateTemplate.startDate = $.format.date(new Date(),
				"yyyy-MM-dd HH:mm:ss");
	} else {
		var temp = startDateText.split("/");
		var startDate = new Date(new Number(temp[2]),
				(new Number(temp[0]) - 1), new Number(temp[1]));
		startDateTemplate.startDate = $.format.date(startDate,
				"yyyy-MM-dd HH:mm:ss");
	}
	
	attributes.push(startDateTemplate);
}
		
		if (tileType == 'task') {

			var attributeMapTemplate = {
				"assignees" : ""
			};

			var assigneesArray = [];
			var mapArray = [];
			for (i = 0; i < selectedToList.length; i++) {
				var map = {};
				var actions = [];
				map[selectedToList[i].id] = actions;
				mapArray.push(map);
			}

			attributeMapTemplate.assignees = mapArray;
			assigneesArray.push(attributeMapTemplate);
			sepecificTileDataTemplate.attributeMaps = assigneesArray;
		}

		sepecificTileDataTemplate.attributes = attributes;
		tileDataTemplate.data = sepecificTileDataTemplate;
		

		
		return tileDataTemplate;
		}
		}


		else{

			return null;
		}
}


/**
 * @param tileType
 * submit the new created tile
 */
function submit(tileType) {
	var tileDataTemplate=null;
	tileDataTemplate = getCustomisedDataForTileCreation(tileType);
	
		if(tileDataTemplate){
		var callBackArguments = {
				"activityid" : tileDataTemplate.activityid,
				"tileType" : tileType
			};
		
		var postData = {"jsondata" : "[" + JSON.stringify(tileDataTemplate) + "]"}

				performAJAX(getCreateTileURL(), "POST", postData, "", submit_callBack,
					callBackArguments, "application/x-www-form-urlencoded");
			
		}
}

/**
 * to display new tile
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */
function submit_callBack(callBackArguments, result, status, xhr) {
	$("#dyna_create_"+callBackArguments.tileType+"_submit").attr("onclick","createTile('"+callBackArguments.tileType+"')");
	if (isSuccess(status)) {
		renderTilesOnView(callBackArguments, result);
	} else {
	}
}

/**
 * to render the data of newly created tiles
 * @param callBackArguments
 * @param result
 */
function renderTilesOnView(callBackArguments, result){

	$(".createJourney_" + callBackArguments.tileType).modal('hide');
	$(".scrib_cmt_scrl").scrollTop(0);
	resetCreateUI(callBackArguments.tileType);
	makeCreateListEmpty();
	  $('.modal-dialog').off('keydown');
	showAlert();
	if(callBackArguments.tileType=='task'){
		$(".success").html("New task assigned in "+getSelectedActivityNameForLightBox(callBackArguments.activityid));	
	}else if(callBackArguments.tileType=='query'){
		$(".success").html("New query asked in  "+getSelectedActivityNameForLightBox(callBackArguments.activityid));	
	}else if(callBackArguments.tileType=='post'){
		$(".success").html("New update posted in  "+getSelectedActivityNameForLightBox(callBackArguments.activityid));
	}

/*
========> Notification after submitting the recruitment form.
*/
	else if(callBackArguments.tileType=='recruitmentform'){	
		$(".success").html("New form posted in  "+getSelectedActivityNameForLightBox(callBackArguments.activityid));

	//else if(callBackArguments.tileType=='leaveform'){	
	//	$(".success").html("New leave applied in  "+getSelectedActivityNameForLightBox(callBackArguments.activityid));
	
		if(goToDrive){
			loadSpecificTiles('drive');
			goToDrive=false;
		}
	} 
	

	var tempTileArray = [];
	var html="";
	tempTileArray[0] = result;
	tileData.push(result);
	tempTileArray=customiseTileData(tempTileArray);
	if ((callBackArguments.activityid == getSelectedActivityId()
			|| activity == HOME_ID)&&(callBackArguments.tileType==tiletype||tiletype=='All')&&starred==false&&
			isFilteredTiles==false) {
	
		/* if this is the first tile create new else prepend */
		if ($("#showTilesOrNotifications").find(".scroles").get(0) == undefined){
			if(view=='gridview'){
				var gridViewTemplate = getTemplate("gridViewTemplate");
				 html = gridViewTemplate(tempTileArray);
				
			$("#showTilesOrNotifications").html(
					"<div class='scroles scrib_container'>" + html + "</div>");
			}
			else{
				var listViewTemplate = getTemplate("listViewTileTemplate");
			     html = listViewTemplate(tempTileArray);
				$("#showTilesOrNotifications").html(
						"<div class='scroles scrib_container'><div class='scrib-margin-list' style='left: 0px; top: 0px'>" + html + "</div></div>");
			}
		}
		else{
			if(view=='gridview') {
				var gridViewTemplate = getTemplate("gridViewTemplate");
				 html = gridViewTemplate(tempTileArray);
				$("#showTilesOrNotifications").find(".scroles").prepend(html);
				rearrangeTiles();
			}
			else{
				var listViewTileTemplate = getTemplate("listView");
			    html=listViewTileTemplate(tempTileArray[0]);
				$("#list_view").prepend(html);
			
			}
			}
			
	}	
}

/**
 * to remove selected user from list
 * @param id
 * @param tileType
 */
function removeFromSelectedTolist(aElem, id, tileType) {
	var tempSelectedToList = [];
	$.each(selectedToList, function(i, item) {
		if (item.id != id)
			tempSelectedToList.push(item);

	});

	selectedToList = tempSelectedToList;
	var elem = $(aElem).parent().parent();
	var ele = elem.find(".cjDetails");
	elem.find(".CJdocuments").remove();
	$(showToIds(selectedToList)).insertBefore(ele);
}
/**
 * 
 * remove tags from list
 * @param aElem
 * @param tag
 * @param tileType
 */
function removeFromSelectedTagslist(aElem, tag, tileType) {

	var tempSelectedTagsList = [];
	$.each(selectedTagsList, function(i, item) {
		if (item.tag != tag)
			tempSelectedTagsList.push(item);

	});

	selectedTagsList = tempSelectedTagsList;
	var elem = $(aElem).parent().parent();
	var ele = elem.find(".cjDetails");
	elem.find(".CJdocuments").remove();
	$(showTagsList(selectedTagsList)).insertBefore(ele);
}

/**
 * to remove uploaded tile from list
 * @param fileName
 * @param divId
 */
function removeUploadedFile(fileName, id, tileType) {
	var tempAttachedIds = [];
	$
			.each(attachedIds,
					function(i, value) {
						if (value.substring(value.lastIndexOf("/") + 1,
								value.length) != fileName) {
							tempAttachedIds.push(value);
						}
					});
	attachedIds = tempAttachedIds;
	$("#upload_" + id).remove();
	rearrangeTiles();
	if (tempAttachedIds.length == 0) {
		$("#dyna_create_" + tileType + "_files").hide();
	}
}

/**
 * to validate the data before create
 * @param tileType
 * @returns {Boolean}
 */
function validateBeforeSubmit(tileType) {
	var validVisibility=isvalidVisibility(tileType);
	var validTitle=validateTitle(tileType);
	if (validTitle && validVisibility)
		return true;
	else
		return false;
}

/**
 * @param tileType
 * to validate the title
 * @returns {Boolean}
 */
function validateTitle(tileType) {
	var title = $.trim($("#dyna_create_" + tileType + "_title").val());
	var skill = $.trim($("#dyna_create_" + tileType + "_skill").val());
	var experience = $.trim($("#dyna_create_" + tileType + "_experience").val());
	var contactnumber = $.trim($("#dyna_create_" + tileType + "_contactnumber").val());
	var stage = $.trim($("#dyna_create_" + tileType + "_stage").val());
	var approver = $.trim($("#dyna_create_" + tileType + "_approver").val());


	var purpose = $.trim($("#dyna_create_" + tileType + "_purpose").val());
	var startdate = $.trim($("#dyna_create_" + tileType + "_startleave").val());
	var duedate = $.trim($("#dyna_create_" + tileType + "_dueleave").val());

	if (title == undefined || title == "" || title.length == 0) {
		$("#dyna_create_" + tileType + "_title").addClass("red");
		$("#dyna_create_" + tileType + "_title").attr("placeholder",
				"Title is missing ");
	
			addPlaceholderClass("#dyna_create_" + tileType + "_title");

		  showplc();
		return false;
	}
/**
===> validating placeholders for recruitment form

**/
	if(tileType=="recruitmentform"){
	if (skill == undefined || skill == "" || skill.length == 0) {
		$("#dyna_create_" + tileType + "_skill").addClass("red");
		$("#dyna_create_" + tileType + "_skill").attr("placeholder",
				"Skill - Set is missing ");
	
			addPlaceholderClass("#dyna_create_" + tileType + "_skill");

		  showplc();
		return false;
		}

	if (experience == undefined || experience == "" || experience.length == 0) {
		$("#dyna_create_" + tileType + "_experience").addClass("red");
		$("#dyna_create_" + tileType + "_experience").attr("placeholder",
				"Years of Experience is missing ");
	
			addPlaceholderClass("#dyna_create_" + tileType + "_experience");

		  showplc();
		return false;
		}

	if (contactnumber == undefined || contactnumber == "" || contactnumber.length == 0) {
		$("#dyna_create_" + tileType + "_contactnumber").addClass("red");
		$("#dyna_create_" + tileType + "_contactnumber").attr("placeholder",
				"Contact Number is missing ");
	
			addPlaceholderClass("#dyna_create_" + tileType + "_contactnumber");

		  showplc();
		return false;
		}

	if (stage == undefined || stage == "" || stage.length == 0) {
		$("#dyna_create_" + tileType + "_stage").addClass("red");
		$("#dyna_create_" + tileType + "_stage").attr("placeholder",
				"Stage Name is missing ");
	
			addPlaceholderClass("#dyna_create_" + tileType + "_stage");

		  showplc();
		return false;
		}

	if (approver == undefined || approver == "" || approver.length == 0) {
		$("#dyna_create_" + tileType + "_approver").addClass("red");
		$("#dyna_create_" + tileType + "_approver").attr("placeholder",
				"Approver Name is missing ");
	
			addPlaceholderClass("#dyna_create_" + tileType + "_approver");

		  showplc();
		return false;
		}
	else
		return true; 	
	}
	
	if(tileType=="leaveform"){
		
	if (purpose == undefined || purpose == "" || purpose.length == 0) {
		$("#dyna_create_" + tileType + "_purpose").addClass("red");
		$("#dyna_create_" + tileType + "_purpose").attr("placeholder",
				"Purpose of leave is missing ");
	
			addPlaceholderClass("#dyna_create_" + tileType + "_purpose");

		  showplc();
		return false;
	}
	if (startdate == undefined || startdate== "" || startdate.length == 0) {
		$("#dyna_create_" + tileType + "_startleave").addClass("red");
		$("#dyna_create_" + tileType + "_startleave").attr("placeholder",
				"From date is missing ");
	
			addPlaceholderClass("#dyna_create_" + tileType + "_startleave");

		  showplc();
		return false;
	}
	if (duedate == undefined || duedate == "" || duedate.length == 0) {
		$("#dyna_create_" + tileType + "_dueleave").addClass("red");
		$("#dyna_create_" + tileType + "_dueleave").attr("placeholder",
				"To date is missing ");
	
			addPlaceholderClass("#dyna_create_" + tileType + "_dueleave");

		  showplc();
		return false;
		}
	else
		return true;	
	}
	 else
		return true;
}

/**
 * @returns {Boolean}
 * to  check is locked or unlocked
 */
function isvalidVisibility(tileType) {
	if(tileType=='task'){		
		if (selectedToList.length == 0) {
			$("#dyna_create_task_assignee").attr("placeholder",
					"Mandatory field for a task");
			$("#dyna_create_task_assignee").addClass("red");
		
				addPlaceholderClass("#dyna_create_task_assignee");
			
			  showplc();
			return false;
		} else
			return true;	
	}
	else
	if ($("#dyna_create_" + tileType + "_lock").is(":visible")) {
		if (selectedToList.length == 0) {
			$("#dyna_create_" + tileType + "_assignee").attr("placeholder",
					"Mandatory field for a private  "+tileType);
			$("#dyna_create_" + tileType + "_assignee").addClass("red");
			
				addPlaceholderClass("#dyna_create_" + tileType + "_assignee");
			
			  showplc();
			return false;
		} else
			return true;
	} 
	else
		return true;
}

/**
 * to add the labels manually
 * @param sourceID
 * @param tileType
 */
function addLabel(sourceID, tileType){	
	$(sourceID)
	.keydown(
			function(e) {
				if (e.keyCode == 13||e.keyCode == 188 ||e.keyCode == 186||e.keyCode == 59) {
					var tag = $(sourceID)
							.val();
									
					if(tag=="") return;
						var item = {
							"tag" : tag,
							"tileType" : tileType
						};
						selectedTagsList.push(item);
						var ele = $(sourceID).parent().parent().find(
								".cjDetails");
						$(sourceID).parent().parent()
								.find(".CJdocuments").remove();
						$(showTagsList(selectedTagsList)).insertBefore(ele);
						if(sourceID=="#post_add_Categories")
							{
								$(this).attr(
							"placeholder", "Add Categories");
							}
						else{
							$(this).attr(
							"placeholder", "Add labels");
							}
				
					$(sourceID).get(0).value = "";
				}
			}).keyup(function(e){
				if (e.keyCode == 13||e.keyCode == 188||e.keyCode == 186||e.keyCode == 59)
					{
					$(sourceID).val("");
					}
	});
	
}
/**
 * to delete the comment from lightbox
 * @param callBackArguments
 * @param callBackMethod
 */

function deleteCommentOnTileORLB(callBackArguments,callBackMethod){		
		performAJAX("/serv/deletecomment/" +callBackArguments.tileId + "/" + callBackArguments.commentId, "GET",
				"", "", callBackMethod, callBackArguments,"application/x-www-form-urlencoded");
}
/**
 * @param commentId
 * to delete the comment
 */
function deleteComment(commentId) {
	var tileId = "";
	var createdby = "";
	var tiletype="";
	$.each(tileData, function(i, tile) {
		$.each(tile.comments, function(i, comment) {
			if (comment.commentid == commentId) {
				tileId = tile.id;
				tiletype=tile.tiletype;
				createdby = comment.authorid;
			}
		});
	});
	var callBackArguments = {
		"tileId" : tileId,
		"commentId" : commentId,
		"tiletype" :tiletype
	};
	if (createdby == getUserId())
		deleteCommentOnTileORLB(callBackArguments,deleteComment_callBack);
		
	else
		alert("you dont have permission to delete this");
}

/**
 * delete comment from lightbox
 * @param commentId
 * @param typeOfTile
 */

function deleteCommentLB(commentId,typeOfTile) {
	var tileId = "";
	var createdby = "";
	$.each(tileLB, function(i, tile) {
		$.each(tile.comments, function(i, comment) {
			if (comment.commentid == commentId) {
				tileId = tile.id;
				createdby = comment.authorid;
			}
		});
	});
	var callBackArguments = {
		"tileId" : tileId,
		"commentId" : commentId,
		"typeOfTile":typeOfTile
	};
	if (createdby == getUserId())
		deleteCommentOnTileORLB(callBackArguments,deleteCommentLB_callBack);
		
	else
		alert("you dont have permission to delete this");
}

/**
 * to handle comment deletion 
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */
function deleteCommentLB_callBack(callBackArguments, result, status, xhr){
	if (isSuccess(status)) {
		showAlert();
		$('.success').html('Reply deleted');
		var allTiles = [];
		var attachmentid = "";
		$.each(tileLB,function(i, tile) {
							if (callBackArguments.tileId == tile.id) {
								var comments = [];
								if (tile.comments.length > 1)
									for (i = 0; i < tile.comments.length; i++) {
										if (tile.comments[i].commentid != callBackArguments.commentId)
											comments.push(tile.comments[i]);
									}

								tile.comments = comments;
								var commentData = {
									"tileid" : tile.id,
									"comments" : comments,
									"tiletype":callBackArguments.typeOfTile
								};
								var commentTemplate=getTemplate("commentBox_"+callBackArguments.typeOfTile+"_Template");
								var commentResults=commentTemplate(commentData.comments);
								$('.commentBoxData_'+callBackArguments.tileId).html(commentResults);								
								$('.commentCount').html(commentData.comments.length);
								
								var gridViewCommentsTemplate = getTemplate("gridViewCommentsTemplate");
								var html = gridViewCommentsTemplate(commentData);
								tile.attachmentList = getAttachmentList(
										tile.attachment, null,tile.id);
								$("#comments_" + tile.id).html(html);						
								var attachmentData = {
									"tileid" : tile.id,
									"attachmentList" : tile.attachmentList
								};								

								var gridViewAttachmentTemplate = getTemplate("gridViewAttachmentTemplate");
								var html = gridViewAttachmentTemplate(attachmentData);
								$("#attachments_" + tile.id).html(html);
							}
						});

	} else {

	}
	attachedIds = [];
	rearrangeTiles();
}
/**
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 * to handle delete comment
 */
function deleteComment_callBack(callBackArguments, result, status, xhr) {
	if (isSuccess(status)) {
		var allTiles = [];
		var attachmentid = "";
		$
				.each(
						tileData,
						function(i, tile) {
							if (callBackArguments.tileId == tile.id) {
								var comments = [];
								if (tile.comments.length > 1)
									for (i = 0; i < tile.comments.length; i++) {
										if (tile.comments[i].commentid != callBackArguments.commentId)
											comments.push(tile.comments[i]);
									}

								tile.comments = comments;
								var commentData = {
									"tileid" : tile.id,
									"comments" : comments,
									"tiletype":callBackArguments.tiletype
								};

								var gridViewCommentsTemplate = getTemplate("gridViewCommentsTemplate");
								var html = gridViewCommentsTemplate(commentData);
								tile.attachmentList = getAttachmentList(
										tile.attachment,null,tile.id);
								$("#comments_" + tile.id).html(html);
								var attachmentData = {
									"tileid" : tile.id,
									"attachmentList" : tile.attachmentList
								};
								var gridViewAttachmentTemplate = getTemplate("gridViewAttachmentTemplate");
								var html = gridViewAttachmentTemplate(attachmentData);
								$("#attachments_" + tile.id).html(html);
							

							}
						});

	} else {
	
	}
	attachedIds = [];
	rearrangeTiles();
}

/**
 * @param type
 * to find the view and navigation
 */
function loadType(type) {
	tileData = [];
	if (view != type) {
		$(window).unbind('scroll');
		$("#showTilesOrNotifications").find(".scroles").remove();
		if(starred){
			view = type;
			loadStarredTiles();
		}
		else{
			
		if (type == 'gridview') {
			$("#grid").attr("class", "active");
			$("#list").attr("class", "list");
			if (tiletype == '*') {
				if (activity == HOME_ID&&activityType=='internal')
					loadTiles('gridview', HOME_ID, 'All');
				else
					loadTiles('gridview', activity, 'All');
			} else {
				if (activity == HOME_ID&&activityType=='internal')
					loadTiles('gridview', HOME_ID, tiletype);
				else
					loadTiles('gridview', activity, tiletype);
			}
		} else {
			$("#grid").attr("class", "grid");
			$("#list").attr("class", "active");
			if (tiletype == 'All') {
				if (activity == HOME_ID&&activityType=='internal')
					loadTiles('listview', HOME_ID, 'All');
				else
					loadTiles('listview', activity, 'All');
			} else {
				
				if (activity == HOME_ID&&activityType=='internal')
					loadTiles('listview', HOME_ID, tiletype);
				else
					loadTiles('listview', activity, tiletype);
			}
		}
	}
}
}

/**
 * 
 * to create the tiletype ui
 * @param tileType
 */
function createTileTypeUi(tileType){
    isFileAttached=false;
    toggleNav();
	if(tileType == 'drive'){
		selectedToList = [];
	    selectedTagsList=[];
		 isFileAttached=true;
		 browseFiles("#selectFile_post");
		 create('post');
		$("#dyna_create_post_files").show();
	}
	else if(tileType == 'smartlist')
		{
		makeCreateListEmpty();
		createSmartList();
		}
	else{
		makeCreateListEmpty();
		create(tileType);
	}
	
	
}




Handlebars.registerHelper('getTaskStatus', function(attributeMaps,isSingleUserTask,id) {

	var response="";
	if(attributeMaps!=null&&attributeMaps!=""&&attributeMaps!=undefined){
			response=getTaskStatus(attributeMaps,isSingleUserTask,id);
	}
			return new Handlebars.SafeString(response);
});

/**
 * to find out the task status
 * @param attributeMaps
 * @param isSingleUserTask
 * @param id
 * @returns {String}
 */

function getTaskStatus(attributeMaps,isSingleUserTask,id){
	var assignee=[];
	var status="";
	var author="";
    var attributeMaps=attributeMaps;
    $.each(tileData,function(i,tile){
    	if(tile.id==id){
    		assignee=tile.recipients;
             status=tile.status;
             author=tile.author;
    		return false;
    		}
    	})
    	return getTaskStatusForSLandTile(assignee, status, author, attributeMaps, isSingleUserTask);
}

function getTaskStatusForSLandTile(assignee, status, author, attributeMaps, isSingleUserTask){
	var response="";
	var userid="";
	var name="";
	var StatusArray=[];
			if(status=='CLOSE'){
				response=" Closed by "+ author.initCap();
				return response;
			}
			else{
				if(assignee==[]||assignee==null||assignee==undefined || assignee == "")
				{
				response="";
				}
				else if(assignee.length>0 && assignee){
					userid=assignee[0].recipientid;
		
					if(isSingleUserTask){
						name=showUser(userid);
						response=getAssigneeValue("progress", attributeMaps,userid);
						if(response==""||response==null||response==undefined){
							response=getAssigneeValue("response", attributeMaps,userid); 
							if(response==null||response==undefined||response=="")
								response=="";
							else
								response=" "+response.initCap()+" by "+name;
						}else
						{
				
							if(response!="100")
								response=" Accepted by "+name+"- "+response+"%";
							else
								response=" Marked 100% complete by "+name;
				 }
		}
		else		   
		if(attributeMaps!=undefined&& attributeMaps!=null && attributeMaps!=""){
		statusArray=getTaskTracker(attributeMaps); 
		if(statusArray.acceptorArray!=null&&statusArray.acceptorArray!=undefined&&statusArray.acceptorArray[0].acceptCount>0){
			if(statusArray.acceptorArray[0].acceptCount==1){
			response=" Accepted by "+statusArray.acceptorArray[0].acceptCount+" user ("+statusArray.acceptorArray[0].accepedBy+")";
			}
			else{
			response=" Accepted by "+statusArray.acceptorArray[0].acceptCount+" users ("+statusArray.acceptorArray[0].accepedBy+")";
			}
		}	
		if(statusArray.rejectorArray!=null&&statusArray.rejectorArray!=undefined&&statusArray.rejectorArray[0].rejectCount>0){
			if(statusArray.rejectorArray[0].rejectCount==1)
			response=response+" Rejected by "+statusArray.rejectorArray[0].rejectCount+" user ("+statusArray.rejectorArray[0].rejectedBy+")";
			else
			response=response+" Rejected by "+statusArray.rejectorArray[0].rejectCount+" users ("+statusArray.rejectorArray[0].rejectedBy+")";
		}
		if(statusArray.needHelpArray!=null&&statusArray.needHelpArray!=undefined&&statusArray.needHelpArray[0].needhelpCount>0){
			if(statusArray.needHelpArray[0].needhelpCount==1)
		    	response=response+" Needhelp by "+statusArray.needHelpArray[0].needhelpCount+" user ("+statusArray.needHelpArray[0].needhelpBy+")";
		    	else
		    	response=response+" Needhelp by  "+statusArray.needHelpArray[0].needhelpCount+" users ("+statusArray.needHelpArray[0].needhelpBy+")";
		}
		}
		else
		response= "";
			}
			else
				response="";
			}
		if(response==""){
			response=" Currently Open";
		}
			return response;
}

String.prototype.initCap = function () {
	   return this.toLowerCase().replace(/(?:^|\s)[a-z]/g, function (m) {
	      return m.toUpperCase();
	   });
	};

	Handlebars.registerHelper('initCap', function(context) {
	return	new Handlebars.SafeString(context.initCap());

	});
	
	
	
/**
 * 
 * to find out the response of single user task
 * @param attributeMaps
 * @param userid
 * @returns {String}
 */
	function getResponseForSingleUserTask(attributeMaps,userid){
		var response="";
		var name=showUser(userid);
		 response=getAssigneeValue("progress", attributeMaps,userid);
    	 if(response==""||response==null||response==undefined){
    		 response=getAssigneeValue("response", attributeMaps,userid); 
    		if(response==null||response==undefined||response=="")
    			response=="";
    		else
    		 response=" - "+response.initCap()+" by "+name;
    	 }else
    		 {
    		
    		 if(response!="100")
    		 response="- Accepted by "+name+"- "+response+"% ";
    		 else
    			 response="- Marked 100% complete by "+name;
    		 }
		return response;
	}
	
	/**
	 * to find out the response of multi user task
	 * 
	 * @param statusArray
	 * @returns {String}
	 */
	function getResponseForMultiUserTask(statusArray){
		var response="";
	    if(statusArray.acceptorArray!=null&&statusArray.acceptorArray!=undefined&&statusArray.acceptorArray[0].acceptCount>0){
	    	if(statusArray.acceptorArray[0].acceptCount==1){
	    	response="- Accepted by "+statusArray.acceptorArray[0].acceptCount+" user ("+statusArray.acceptorArray[0].accepedBy+")";
	    	}
	    	else{
	    	response="- Accepted by "+statusArray.acceptorArray[0].acceptCount+" users ("+statusArray.acceptorArray[0].accepedBy+")";
	    	}
	    }	
	    if(statusArray.rejectorArray!=null&&statusArray.rejectorArray!=undefined&&statusArray.rejectorArray[0].rejectCount>0){
	    	if(statusArray.rejectorArray[0].rejectCount==1)
	    	response=response+"- Rejected by "+statusArray.rejectorArray[0].rejectCount+" user ("+statusArray.rejectorArray[0].rejectedBy+")";
	    	else
	    	response=response+"- Rejected by "+statusArray.rejectorArray[0].rejectCount+" users ("+statusArray.rejectorArray[0].rejectedBy+")";
	    }
	    if(statusArray.needHelpArray!=null&&statusArray.needHelpArray!=undefined&&statusArray.needHelpArray[0].needhelpCount>0){
	    	if(statusArray.needHelpArray[0].needhelpCount==1)
	        	response=response+"- Needhelp by "+statusArray.needHelpArray[0].needhelpCount+" user ("+statusArray.needHelpArray[0].needhelpBy+")";
	        	else
	        	response=response+"- Needhelp by  "+statusArray.needHelpArray[0].needhelpCount+" users ("+statusArray.needHelpArray[0].needhelpBy+")";
	    }
		    return response;
		 }
	/**
	 *  to get the query statsus
	 * 
	 */
	

Handlebars.registerHelper('getQueryStatus', function(id,attributes) {
	var response="";
	var answersCount="";
	var status="";
	var author="";
	$.each(tileData,function(i,tile){
    	if(tile.id==id){
             status=tile.status;
             author=tile.author;
    		return false;
    		}
    	})
 
    	if(status=="CLOSE"){
    		response=" Answered  ";
    	}

    	else{
    	if(attributes!=null){
	$.each(attributes,function(j,attribute){
		$.each(attribute,function(key,value){
			if(key == 'answers'){
				response="answers";
				answersCount=value;
			}
		});
	});
    	}
	   if(response=="answers" && answersCount!=0){
		   if(answersCount==1){
			   response=" "+answersCount+" Answer Recieved";
		   }
		   else{
			   response=" "+answersCount+" Answers Recieved";
		   }
		
	   }
	   else{
		   response=" Currently open";
	   }
    	}
return new Handlebars.SafeString(response);

});

/**
 * 
 * To check Tile asigned to single person or multiple persons
 * @param tile
 * @returns {Boolean}
 */
function isSingleUserTile(tile){
	var response="";
	if(tile.tiletype=="task"){
	    
	    if(tile.data!=undefined && tile.data.attributeMaps!=undefined){
	    for (var assignee in tile.data.attributeMaps) {
	        var links = tile.data.attributeMaps[assignee];
	        for(var link in links){
	                var assign=links[link];
	                if(assign.length>1)
	                  return false;
	                else
	                	return true;
	        }
	    }        
		
	}

}
	else
		return false;
}

/**
 * 
 * To check the author and login user is same or not
 * @param authorid
 * @returns {Boolean}
 */
function isInitiator(authorid){	
	if(authorid==getUserId())
		return true;
	else
	  return false;
}

/**
 * 
 *  To find out login user is assignee or viewer
 * @param recipients
 * @returns {String}
 */
function isTaskAssigned(recipients){
	var taskadded="false"
	if(recipients!=null&&recipients!=undefined&&recipients!=""){
	for(var i=0;i<recipients.length;i++){
		if(recipients[i].recipientid==getUserId()){
			taskadded="true";
			break;
		}
	}
		
}
return taskadded;	
		
}

/**
 * 
 *  To forward a tile
 * @param tileid
 * @param tileType
 */

function forward(tileid,tileType){
	jQuery('#'+tileid).dialog('close');
    create(tileType);
    tileData=customiseTileData(tileData);
    $.each(tileData,function(i,tile){
            if(tile.id == tileid){
                    $("#dyna_create_"+tileType+"_title").get(0).value=capitalizeFirstLetter(tile.title);
                   
                    $(".AddDet").click();
                    if(tile.teaser){
                    $("#dyna_create_"+tileType+"_description").get(0).value=capitalizeFirstLetter(tile.teaser);
                    }
                    $("#dyna_create_"+tileType+"_description").autosize().show().trigger('autosize.resize');
                    return false;
            }
    });
    
    var height=   $("#dyna_create_"+tileType+"_description").height();
    
    height=height/2.7;
    $("#dyna_create_"+tileType+"_description").css("height",height);
}

/**
 * to get duedatefrom attributes 
 * @param attributes
 * @param givenKey
 * @returns {String}
 */
function getDueDateFromDataAttributesForward(attributes, givenKey) {
	var givenKeyValue = "";
	$.each(attributes,function(i,object){
	$.each(object, function(key, value) {
		if (key == givenKey)
			givenKeyValue = value;
	});
	});
	return givenKeyValue;
}



/**
 * handlebar for list view(inside li template)
 * 
 */

Handlebars.registerHelper('showList', function(id,author,comments,attachmentList,
		data,response,isSingleUserTask,isTaskAdded,tiletype,authorid,isCreator,
		progress,isClosed,action,isAvatarFound,userResponse) {
	
	var tile={
			"id":id,
			"author":author,
			"comments":comments,
			"attachmentList":attachmentList,
			"data":data,
			"response":response,
			"isSingleUserTask":isSingleUserTask,
			"isTaskAdded":isTaskAdded,
			"tiletype":tiletype,
			"authorid":authorid,
			"isCreator":isCreator,
			"progress":progress,
			"isClosed":isClosed,
			"action":action,
			"isAvatarFound":isAvatarFound,
			"userResponse":userResponse
			
	}
	var ListViewTemplate = getTemplate("ListViewTemplate");
	var html = ListViewTemplate(tile);	
	return new Handlebars.SafeString(html);
});


/**
 * handlebar for list view (to add lis on ul)
 * 
 */


Handlebars.registerHelper('showTile', function(title,activityid,id,avatar,author,
		createddate,comments,attachmentList,data,
		response,isSingleUserTask,isTaskAdded,
		tiletype,authorid,isCreator,
		progress,isClosed,action,isAvatarFound,userResponse,teaser) {
	
	var tile={
			"createddate":createddate,
			"avatar":avatar,
			"activityid":activityid,
			"id":id,
			"author":author,
			"comments":comments,
			"attachmentList":attachmentList,
			"data":data,
			"response":response,
			"isSingleUserTask":isSingleUserTask,
			"isTaskAdded":isTaskAdded,
			"tiletype":tiletype,
			"authorid":authorid,
			"title":title,
			"isCreator":isCreator,
			"progress":progress,
			"isClosed":isClosed,
			"action":action,
			"isAvatarFound":isAvatarFound,
			"userResponse":userResponse,
			"teaser":teaser
			
	}
	var listViewTemplate = getTemplate("listView");
	var html = listViewTemplate(tile);	
	return new Handlebars.SafeString(html);
});



/**
 * to get the file name from file path
 * 
 */

	Handlebars.registerHelper('getFileName', function(context){
	    if(context){
	      var ret = "";
	          
	      var tempArr = context.trim().split("/");

	      for(var i=0;i<tempArr.length;i++)
	      {   
	          if(i==tempArr.length-1)
	          ret =tempArr[i];
	                     
	      }

	      return ret;
	    }
	  });

	
	
	/**
	 * to return replies or comments as a text
	 * 
	 */
	
	Handlebars.registerHelper('getMessage', function(tiletype,length){
		 var message="";
			if(tiletype=='query'){
		      if(length==1)
		    	message=length+" Answer ";
		      else
		    	  message=length+" Answers ";
		    }
		    else{
		    	 if(length==1)
		    		 message=length+" Reply ";
			      else
			    	  message=length+" Replies ";
		    }
			return new Handlebars.SafeString(message);
		  });
	/**
	 * To return task status for multiple assignee tasks
	 * 
	 * @param attributeMaps
	 * @returns
	 */

	function getTaskTracker(attributeMaps){	
		var acceptCount=0;
		var rejectCount=0;
		var needhelpCount=0;
		
		var accepedBy="";
		var rejectedBy="";
		var needhelpBy="";
		
		var acceptorArray=new Array();
		var rejectorArray=new Array();
		var needHelpArray=new Array();
		
		var statusArray=new Object();
		var assigneeCount=0;
		$.each(attributeMaps,function(j,assignees){
			$.each(assignees,function(k,assignee){
				$.each(assignee,function(l,list){					
					$.each(list,function(m,links){						
						$.each(links,function(n,register){
							if(register.response=="accepted"){
								acceptCount++;
							    accepedBy=accepedBy+", "+showUser(Object.keys(list));
							var response=getAssigneeValue("progress", attributeMaps,Object.keys(list));
							 if(response!=""&&response!=null&&response!=undefined){
								  accepedBy=accepedBy+ " -" +response+"%" ;
							 }
						
							
							}
							if(register.response=="rejected"){
							   rejectCount++;
							   rejectedBy=rejectedBy+","+showUser(Object.keys(list));
							}
							if(register.response=="needhelp"){
								needhelpCount++;
								needhelpBy=needhelpBy+","+showUser(Object.keys(list));
							}
							
						});
					});
				});
			});
		});
		if(acceptCount!=0||rejectCount!=0||needhelpCount!=0){	
			accepedBy=accepedBy.substring(accepedBy.indexOf(",")+1,accepedBy.length);
			rejectedBy=rejectedBy.substring(rejectedBy.indexOf(",")+1,rejectedBy.length);
			needhelpBy=needhelpBy.substring(needhelpBy.indexOf(",")+1,needhelpBy.length);
			
		acceptorArray.push({"acceptCount":acceptCount,"accepedBy":accepedBy});
		rejectorArray.push({"rejectCount":rejectCount,"rejectedBy":rejectedBy});
		needHelpArray.push({"needhelpCount":needhelpCount,"needhelpBy":needhelpBy});
		
		statusArray.acceptorArray=acceptorArray;
		statusArray.rejectorArray=rejectorArray;
		statusArray.needHelpArray=needHelpArray;
		
		
		return statusArray;
		}
		else
			return "";
	}

	
	
	/**
	 * To close or reopen the tasks and querys
	 * 
	 * @param id
	 * @param status
	 */
	
	
	
function CloseOrReOpenTile(id,status,tileType){
	var url="";
	if(status=='Close')
		 url=getCloseTileUrl(id);
	else
		 url=getReOpenTileUrl(id);
	
	var callBackArguments={
			"status":status,
			"id":id,
			"tileType":tileType
			
	}
	performAJAX(url, "GET", "", "", CloseOrReOpenTile_callBack,callBackArguments, "");		
}

/**
 * callback function to repaint  after action success
 * 
 * @param callBackArguments
 * @param result
 * @param status
 * @param xhr
 */

function CloseOrReOpenTile_callBack(callBackArguments,result, status, xhr){
	if (isSuccess(status)){
		if(!tileTobeCreated){
			reLoadSmartList(getListId());
		}
		 clearTimeout(callupdates);
		viewUpdates();
		id=callBackArguments.id;
	
		if(callBackArguments.status=='Close'){
		$("#tile_status_action_"+id).attr("onclick","javascript:CloseOrReOpenTile('"+id+"','Reopen','"+callBackArguments.tileType+"')");
		$("#status_grid_action_"+id).html(' Reopen');
		$(".task_list_action_"+id).html("<span onclick=javascript:CloseOrReOpenTile('"+id+"','Reopen','"+callBackArguments.tileType+"') id='task_list_Reopen_"+id+"' class='scrib-link-over'>Reopen</span>");
		$(".lightbox_"+callBackArguments.tileType+"_list_action_"+id).html("<h6 onclick=javascript:CloseOrReOpenTile('"+id+"','Reopen','"+callBackArguments.tileType+"') id='task_list_Reopen_"+id+"' class='scrib-x2 task_list_Reopen_"+id+"'>Reopen</h6>");
		
		$('#status_'+id).text(" Closed");
		$('#status_lightbox_'+id).text(" Closed");
		showAlert();
		$(".success").html( callBackArguments.tileType.initCap()+"   closed");
		showAlertOnLB();
		$(".successLB").html(callBackArguments.tileType.initCap()+"    closed");
		}else{
		
			$("#tile_status_action_"+id).attr("onclick","javascript:CloseOrReOpenTile('"+id+"','Close','"+callBackArguments.tileType+"')");
			$("#status_grid_action_"+id).html(' Close');
			$(".task_list_action_"+id).html("<span class='scrib-link-over' onclick=javascript:CloseOrReOpenTile('"+id+"','Close','"+callBackArguments.tileType+"') id='task_list_Close_"+id+"'>Close</span>");	
			
			$(".lightbox_"+callBackArguments.tileType+"_list_action_"+id).html("<h6 onclick=javascript:CloseOrReOpenTile('"+id+"','Close','"+callBackArguments.tileType+"') id='task_list_Close_"+id+"' class='scrib-x2 task_list_Close_"+id+"'>Close</h6>");
			
			$('#status_'+id).text(" Re-Opened");
			$('#status_lightbox_'+id).text(" Re-Opened");
			showAlert();
			$(".success").html(callBackArguments.tileType.initCap()+"  re-opened");
			showAlertOnLB();
			$(".successLB").html(callBackArguments.tileType.initCap()+"   re-opened");
			
		}
	}
}


/**
 * to find out the tiletype is either task or query
 * 
 */

Handlebars.registerHelper('ifTaskorQuery', function(tiletype,options) {
	if(tiletype=='task'||tiletype=='query'){
		 return options.fn(this);
	}
	else{
		return options.inverse(this);
	}
});


Handlebars.registerHelper('ifTaskorPost', function(tiletype,options) {
	if(tiletype=='task'||tiletype=='post'){
		 return options.fn(this);
	}
	else{
		return options.inverse(this);
	}
});



/**
 * to set due date  for query
 * @param tileType
 * @param tileId
 * @param dueDateId
 */
function setDueDate(tileType,tileId,dueDateId){
var maxDateString = $.format.date(new Date(), DEFAULT_DATE_FORMAT);
var tempmaxDate = new Date(maxDateString);
tempmaxDate.setDate(tempmaxDate.getDate() + 14);
var maxdate = tempmaxDate;

$(dueDateId).datepicker(
		{ 	
			showOn: 'button',
	        buttonText: 'Change',
			minDate : new Date,
			maxDate:maxdate,
			onClose : function(selectedDate) {					
				if (selectedDate != showDueDate(getDueDate(tileId))) {					
					var temp = selectedDate.split("/");
					var currentDate = new Date(new Number(temp[2]),
							(new Number(temp[0]) - 1), new Number(
									temp[1]));
					var ms = (currentDate).getTime();
					
					var dueDate=$.format.date(new Date(ms),DEFAULT_DATE_FORMAT);					
					updateDueDate(tileId,tileType,dueDateId,dueDate);
					dueDate=$.format.date(new Date(ms),"yyyy-MM-dd HH:mm:ss");
					$(dueDateId).val(showDueDate(dueDate));
					$('#dummyspan_'+tileId).text($(dueDateId).val()+"c");
					$(dueDateId).css("width",$('#dummyspan_'+tileId).width());


					
				
					$("#dueDate_"+tileId).html(" "+showDueDate(dueDate));
				}						
			}
		});

}

/**
 * handlebar to return description in list view
 * 
 */

Handlebars.registerHelper('showDescription', function(title,teaser) {
	var titleLength=title.length;
	var desc="";
	if(titleLength<300&&teaser!=null&&teaser!=""){
		var descShownLength=297-titleLength;
		teaser=teaser.replace(/(<([^>]+)>)/ig,"");
		desc="-"+teaser.substring(0,descShownLength);
		if(descShownLength<40){
		 desc="-"+teaser.substring(0,40);
		}
		if(desc.length+title.length>297){
	    desc=desc+"..";
	}
	}
	return new Handlebars.SafeString(desc);
});




/****************************New Tile View  Specific Js*****************************************/

/**
 * to return activity specific text
 * 
 */
Handlebars.registerHelper('getActivityText',function(activityid,activityassigned) {
	var text="";

	if(activityassigned=='Y'){
	text+="<i class='fa fa-unlock-alt scrib-x4  '></i> ";
	}
	else{
		text+="<i class='fa fa-lock scrib-x4'></i> "
	}
    if(activityid==HOME_ID){
		text+="  <span >Shared <span class='scrib-link-over'  onClick=javascript:changeUrl('All','"+activityid+"')>Publicly</span></span> ";
	}
	else{
		text+="  <span >Shared within <span class='scrib-link-over' onClick=javascript:changeUrl('All','"+activityid+"')>"+capitalizeFirstLetter(getSelectedActivityName(activityid))+"</span> </span>";
	}
	

	return new Handlebars.SafeString(text);
});

/**
 * to return tiletype  specific actions
 * 
 */
Handlebars.registerHelper('showActions',function(tileType,tileId, authorId, attributeMaps,isNotBookmarked,status) {
	var sb="";
	if(tileType=='post'||tileType=='query'){
		sb=showMore(tileId,tileType,isNotBookmarked,status,authorId);
}
	if(tileType=='task'){
		sb="";
		if(status!="CLOSE"){
		sb=showTaskActions(tileId, authorId, attributeMaps);
		}
		if(sb!=""){
			var bookmarkOption=showBookmarkOptions(isNotBookmarked,tileId,tileType);
			var forwrdOption=showForwardOption(tileId,tileType);
			var closeorOpenOption=showCloseorOpenOption(tileId,status,tileType);
		sb+="<span  id='task_more_"
				+ tileId +"'class='btn-group scrib-link-over'>" +
						" <span type='button' class='dropdown-toggle' data-toggle='dropdown'>" +
						"<strong>More</strong></span>  " +
						"<ul class='dropdown-menu' style='width:150px; margin-left:-85px; top:25px;' role='menu' style='width:250px;'> <li><a>"+bookmarkOption+"</a></li><li><a>"+forwrdOption+"</a></li> </ul></span>";
		
		}
		else{
			sb+=showMore(tileId,tileType,isNotBookmarked,status,authorId);
		}
	}
	
		return new Handlebars.SafeString(sb);
	});


/**
 * 
 * to show more actions for tiles
 * @param tileId
 * @param tileType
 * @param isNotBookmarked
 * @param status
 * @param authorId
 * @returns {String}
 */

function showMore(tileId,tileType,isNotBookmarked,status,authorId){
	var sb="";
   sb+=showBookmarkOptions(isNotBookmarked,tileId,tileType);
   sb+=showForwardOption(tileId,tileType);
   if(tileType!='post'&&authorId==getUserId()){
   sb+=showCloseorOpenOption(tileId,status,tileType);
   }
   return sb;	
}

/**
 * to return bookmark or unbookmark options
 * @param isNotBookmarked
 * @param tileId
 * @returns {String}
 */

function showBookmarkOptions(isNotBookmarked,tileId,tileType){
	var sb="";
	if(isNotBookmarked){
		
		sb+="<span id='tile_bookmark_"
			+ tileId+"'class='scrib-labels ' onclick=javascript:addTileToPod('"
			+ tileId + "','"+tileType+"')><strong><span id='bookmark_"+tileId+"' class='scrib-link-over'>Bookmark</span></strong></span>";
			}else{
				sb+="<span   id='tile_bookmark_"
				+ tileId+"'class='scrib-labels ' onclick=javascript:deleteTileFromPod('"
				+ tileId + "','"+tileType+"')><strong><span id='bookmark_"+tileId+"' class='scrib-link-over'>Un-Bookmark</span></strong></span>";
			}
	return sb;	
}

/**
 * to show tile forward option
 * @param tileId
 * @param tileType
 * @returns {String}
 */

function showForwardOption(tileId,tileType){
	var sb="";
	   sb+="<span  id='"+tileType+"_forward_"
		+ tileId+"'class='scrib-labels scrib-link-over' data-toggle='modal' data-target='.createJourney_"+tileType+"'    onclick=javascript:forward('"
		+ tileId + "','"+tileType+"')><strong>Forward</strong></span>";
	 	return sb;	
}

/**
 * 
 * to show tile close or open option
 * @param tileId
 * @param status
 * @returns {String}
 */

function showCloseorOpenOption(tileId,status,tileType){
	var sb="";
  var action='Close';
    if(status=='CLOSE'){
    	action="Reopen";
    }else{
    	action='Close';
    }
	sb+="<span id='tile_status_action_"
		+ tileId+"'class='scrib-labels ' onclick=javascript:CloseOrReOpenTile('"+tileId+"','"+action+"','"+tileType+"')>" +
				"<strong>" +
				"<span id='status_grid_action_"+tileId+"' class='scrib-link-over'> "+action+"</span></strong></span>";
		
	return sb;
}

/**
 *  to return assignees list 
 * 
 */
	
	Handlebars.registerHelper('showAssignees',function(recipients){
		var data={
				"recipients":recipients
		}
		var assigneeTemplate=getTemplate("lightBox_assigneeTemplate");
		var assigneeData=assigneeTemplate(data);
		assigneeData=assigneeData.substring(0,assigneeData.lastIndexOf(','));
		return new Handlebars.SafeString(assigneeData);
		});
	
	/**
	 * to return tags
	 * 
	 */
	
	
	Handlebars.registerHelper('showTags',function(tags,activityid){
		var data={
				"tags":tags,
				"activityid":activityid
		}
	var tagsTemplate=getTemplate("lightBox_tagsTemplate");
	var tagsData=tagsTemplate(data);
	return new Handlebars.SafeString(tagsData);
	});
	
	/**
	 * to hide given id html element
	 * @param id
	 */
	
	function hideThis(id){
		$(id).hide();
	}
	
	
	 Handlebars.registerHelper('enhance', function(text) {
		 var protocol = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
		      var scheme   = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
		 if(text){
			 text = text.replace(/<br\s*\/?>/mg,"\r\n");     
		 }
		 text = Handlebars.Utils.escapeExpression(text);
			text = text.replace( protocol, '<a href="$1" target="_blank">$1</a>');
		text = text.replace( scheme,   '$1<a href="http://$2" target="_blank">$2</a>' );
			text=capitalizeFirstLetter(text);
			return  new Handlebars.SafeString(text);
		    });
	 /**
	  * to remove older versions on create and delete
	  * @param tileAttachments
	  * @param commentAttachments
	  * @param id
	  * @returns
	  */
	 
	 function removeOlderVersions(tileAttachments,commentAttachments,id){
		 var tileAttachmentsList=getAttachmentList(tileAttachments,null,id);
		 var commentAttachmentsList=getAttachmentList(null,commentAttachments,id);
		 var count=0;
		 var nonDuplicates=[];
		 $.each(tileAttachmentsList,function(i,attachment){
				$.each(commentAttachmentsList,function(j,attachment1){
					if(attachment.fileName==attachment1.fileName){
						count++;
					}  
				});
				if(count==0){
					nonDuplicates.push(attachment);
				}
				
					count=0;
				});
		 $.each(nonDuplicates,function(i,attachment){
			 commentAttachmentsList.push(attachment);
		 })
		 
		 return commentAttachmentsList;
	 }
	 
	 
	 Handlebars.registerHelper('showAttachmentText',function(attachCount){
		
		if(attachCount==1){
			return  new Handlebars.SafeString("  1 Attachment");
		}else{
			return  new Handlebars.SafeString("  "+attachCount+" Attachments");
		}
	});
	 
	 /**
  * to get tiletype icons 
	  * @param value
	  * @returns {String}
	  */
	 function getTileTypeIcons(value){
		 if(value=='post'){
		return  "scrib_hover_fix creat-new-post glyphicon glyphicon-pencil";
		 }
		 if(value=='task'){
			 return 	 "creat-new-task fa fa-check"
		 }
		 if(value=='query'){
			 return "creat-new-query fa fa-question-circle fa-2";
		 }
		 if(value=='delete'){
			 return "creat-new-drive glyphicon glyphicon-folder-open"
		 }
	 }
	 
	 
	 Handlebars.registerHelper('log',function(text){
		
		console.log(text);
	});

	  function uploadAttachment(fd,statusDivId,tileType){
		 
			$
			.ajax({
				url : getUploadFileURL(),
				type : "POST",
				data : fd,
				contentType : false,
				processData : false,
				cache : false,
				  beforeSend: function() {
					  var html="<span  id='upload'><img src='assets/images/loader.gif' ></span>";
					  $(statusDivId).append(html);
				  },
		            complete: function() {
		            },
				success : function(data) {
                                                                
					  attachedIds.push(data[0]);
                                                
					var jsonTemplate = {
						"fileName" : "",
						"id" : ""
					};
					var position = data[0].lastIndexOf("/");
					jsonTemplate.filePath = data[0];
					jsonTemplate.fileName = data[0].substring(
							position + 1, data[0].length);
					jsonTemplate.id = randomString(); 
					jsonTemplate.fileType = jsonTemplate.fileName
							.substring(jsonTemplate.fileName
									.lastIndexOf(".") + 1,
									jsonTemplate.fileName.length);
					jsonTemplate.tileType = tileType;
					$(statusDivId).find("#upload").remove();
					if (statusDivId.indexOf("uploadFileStatus") != -1) {
						var uploadFileStatusCommentTemplate = getTemplate("uploadFileStatusCommentTemplate");
						var htmlContent = uploadFileStatusCommentTemplate(jsonTemplate);
						$(statusDivId).append(htmlContent);
						$(statusDivId).show();
						rearrangeTiles();
					} else {
						var uploadFileStatusTemplate = getTemplate("uploadFileStatusTemplate");
						var htmlContent = uploadFileStatusTemplate(jsonTemplate);

						var ele = $(statusDivId).find(".clearfix");
						$(htmlContent).insertBefore(ele);
					}

				},
				error : function(xhr, status, error) {
					$(statusDivId).find("#upload").remove();
				//	alert(" TO DO When Upload Failed !! ");
				}
			});
 
		 
	 }
	  
	  function removePlaceholderClass(id){
		  if(!placeholderSupport){
		  $(id).removeClass("redie");
		  }
	  }
	  function addPlaceholderClass(id){
		  if(!placeholderSupport){
		  $(id).addClass("redie");
		  }
	  }
	  