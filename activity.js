/**
 * Paint the activity drop down with count on task and query
 */

var TileTypeArray=['post','task','query','drive','recruitmentform','leaveform','expensereport','dashboard','members','smartlist','All'];

/**
 * It is to 


  Tiletype object 
 * 
 * 
 */
function TileType(type,count,Title,id,isTileType,classNames){
	this.type=type;
	this.Count=count;
	this.Title=Title;
	this.id=id;
	this.isTileType=isTileType;
	this.classNames=classNames;
}

/**
 * to create activity
 * 
 */
function CreateandInvite(id,target,functionName,displayName){
	this.id=id;
	this.target=target;
	this.functionName=functionName;
	this.displayName=displayName;
	
	
}


var tempActivitiesData = new Array();
var tempHome=[];
var index="";
var CreateandInviteArray=[];

/**
 * to show activities and update counts 
 * @param action
 * @param type
 */
function loadActivities(action,type){	
	var text="Navigate to External Network";
	var loadType="external";
	if(type=="external"){
		text="Navigate to Internal Network";
		loadType="internal";
		activityType="external"
	}
	 var homeTaskCount=0;
     var homeQueryCount=0;
     var homeCount=0;
     
	$.each(activitiesData,function(i,activity){		
		          var activityData = new Object();
		           activityData.id=activity.id;
					if(activity.id == HOME_ID){
					activityData.title="Public";
					}
					else{	
			
						activityData.title=capitalizeFirstLetter(activity.name);
						
					}
					activityData.taskCount=0;					
					activityData.queryCount=0;					
					$.each(perspectiveData, function(key, value) {
						if(key == activity.id )
						$.each(value, function(innerKey, innerValue) {
							if(innerKey == TASK){
								activityData.taskCount = activityData.taskCount+ parseInt(innerValue);
								homeTaskCount+=activityData.taskCount;
							}
							else {
								activityData.queryCount = activityData.queryCount+ parseInt(innerValue);
						homeQueryCount+=activityData.queryCount; 
							}});
					});	
					
					activityData.Count=activityData.taskCount+activityData.queryCount;
					homeCount+=activityData.Count;
						if(action=="update"){
							if(activityData.taskCount>0){
								
						$(".Count_task_"+activity.id).html(activityData.taskCount);
							}	
						if(activityData.queryCount>0){
						
							$(".Count_query_"+activity.id).html(activityData.queryCount);
						}
						
						activityData.Count=activityData.taskCount+activityData.queryCount;
						if(activityData.Count>0){
						$(".count_"+activity.id).html(activityData.Count);
						
						}
						}
						tempActivitiesData.push(activityData);				
			});
	
	
	$.each(tempActivitiesData,function(i,activity){
		if(activity.id==HOME_ID){
			activity.taskCount=homeTaskCount;
			activity.queryCount=homeQueryCount;
			activity.Count=homeCount;
			tempActivitiesData[i]=activity;
	}		
		});
	
	if(action=="update"){
		if(homeTaskCount>0){
		$(".Count_task_"+HOME_ID).html(homeTaskCount);
		}
		if(homeQueryCount>0){
		$(".Count_query_"+HOME_ID).html(homeQueryCount);
		}
		if(homeCount>0){
		$(".count_"+HOME_ID).html(homeCount);
		}
	}
	
	if(action!="update"){
		var html="";
		var html1="";
	
	var accordionTemplate = getTemplate("accordionTemplateInbox");
	var accordianTemplateMobile=getTemplate("accordionTemplateInboxForMobile");
	 html += accordionTemplate(tempActivitiesData);
	html1 += accordianTemplateMobile(tempActivitiesData);
	$("#section_inbox").html("");
	$("#section_inbox_mobile").html("");
	var inviteHtml="";
	if(accountType!="Community"){
		inviteHtml+=getInternalorExternalActivityHtml(loadType,text);
	}
	inviteHtml+=getCreateActivityandInviteHtml();
	$(".scrib-bottom-work").html(inviteHtml)
	
	}
	$("#section_inbox").html(html);	
	$("#section_inbox_mobile").html(html1);	
	
	$('.scrib_external_ws').tooltip();
	tempActivitiesData=[];
	CreateandInviteArray=[];
}

/**
 * to show perspectives
 * 
 */
Handlebars.registerHelper('showPerspectives', function(id){
	var taskCount=0;
	var queryCount=0;
	var inboxObject=[];
	var html ="";
	$.each(tempActivitiesData,function(i,activityData){
		if(activityData.id==id){
			taskCount=activityData.taskCount;
			queryCount=activityData.queryCount;
		}
	});		
 
	inboxObject.push(new TileType('post',"",'Post',id,true,"scrib_hover_fix creat-new-post glyphicon glyphicon-pencil"));
	inboxObject.push(new TileType('task',taskCount,'Task',id,true,"scrib_hover_fix creat-new-task fa fa-check"));
	inboxObject.push(new TileType('query',queryCount,'Query',id,true,"scrib_hover_fix creat-new-query fa fa-question-circle fa-2"));
	inboxObject.push(new TileType('drive',"",'Files',id,true,"scrib_hover_fix creat-new-drive glyphicon glyphicon-folder-open"));
	inboxObject.push(new TileType('smartlist',"",'Smartlists',id,true,"scrib_hover_fix creat-new-todo fa fa-th-list"));
	
	if(id!=HOME_ID){
	inboxObject.push(new TileType('recruitmentform',"",'Recruitment Form',id,true,"scrib_hover_fix creat-new-form glyphicon glyphicon-pencil"));
	inboxObject.push(new TileType('leaveform',"",'Leave Form',id,true,"scrib_hover_fix creat-new-leave fa fa-file-text"));
	inboxObject.push(new TileType('expensereport',"",'Expense Report',id,true,"scrib_hover_fix creat-new-report glyphicon glyphicon-usd"));
		
	inboxObject.push(new TileType('dashboard',"",'Dashboard',id,false,"scrib_hover_fix fa fa-bar-chart-o scrib_db"));
	inboxObject.push(new TileType('members',"",'Members',id,false,"scrib_hover_fix  scrib_users_bg"));
	}
	
	var showTypeTemplate=getTemplate("inBoxTypeTemplate");
	$.each(inboxObject,function(i,type){
		html=html+showTypeTemplate(type);
	});

    return new Handlebars.SafeString(html);
});




/**
 * to get create activity and invite users html
 * @returns {String}
 */


function getCreateActivityandInviteHtml(){
	var html="";

		if(activityType!='external'){
			var contacts=new CreateandInvite('inviteMembers','.invite_members','invitation()','Members')
			var contactsTemplate=getTemplate("contactsTemplate");
			html+=contactsTemplate(contacts);
	}
	var createActivityTemplate=getTemplate("createActivityandInviteUsersTemplate");
	var createactivity=new CreateandInvite('createActivity','.create_activity','createNewActivity()','Create New Workspace') ;
		html+=createActivityTemplate(createactivity);

	return html;
}

/**
 * to get internal or eXternal accordian
 * @param loadType
 * @param text
 * @returns {String}
 */
function getInternalorExternalActivityHtml(loadType,text){
	var html="";
	var addClass;
	if(activityType == 'internal'){ addClass = "icon-external-activity";}else{addClass = "icon-internal-activity";}
	var internalorExternalActivityTemplate=getTemplate("internalorExternalActivityTemplate");
	var externalorInternalData={
			"type":loadType,
			"text":text,
			"addingClass":addClass
	}
	html+=internalorExternalActivityTemplate(externalorInternalData);
	return html;
}

/**
 * to hide Currently open activity tiles count
 */

function showTilesCount(){
$.each(activitiesData,function(i,activityObject){
	if(activityObject.id==activity){
		$(".count_"+activity).attr("style","display:none");
	}else{
		$(".count_"+activityObject.id).attr("style","display:block");
	}
})
}


/**
 *  to load external or internal activities 
 * @param type
 */

function loadActivityTypes(type,isFromHistory){
	toggleNav();
	var callBackArguments={
			"type":type,
			"isFromHistory":isFromHistory
	}
	
	performAJAX(getActivitiesURl(getUserId(),type,"open",HOME_ID), "get", "", "",
			loadActivityTypes_callBack, callBackArguments,
			"application/x-www-form-urlencoded");
	
}

/**
 * to paint external activities
 * @param type
 * @param result
 * @param status
 * @param xhr
 */



function loadActivityTypes_callBack(callBackArguments,result, status, xhr){
	setAllActivities(result);
	showCommonHeader();
	activityType=callBackArguments.type;
	if(activitiesData.length!=0)
		{
		 if(activityType == 'external') 
			{
			$("#dyna_searchText_portable").attr('placeholder','Search in "'+activitiesData[0].name+'"');
			$("#dyna_searchText_mobile").attr('placeholder','Search in "'+activitiesData[0].name+'"');
			bindAutoSearch('#dyna_searchText_portable','#auto-search-portable',activitiesData[0].id);
 			bindAutoSearch('#dyna_searchText_mobile','#auto-search-mobile',activitiesData[0].id);
			}
		else
			{
			$("#dyna_searchText_portable").attr('placeholder','Search in all workspaces');
			$("#dyna_searchText_mobile").attr('placeholder','Search in all workspaces');
			callBindAutoSearch(HOME_ID);
			}
		searchinactivity=activitiesData[0].id;
		}
	else
		{
		$("#dyna_searchText_portable").attr('placeholder','Search');
		$("#dyna_searchText_mobile").attr('placeholder','Search');
		bindAutoSearch('#dyna_searchText_portable','#auto-search-portable',"");
 		bindAutoSearch('#dyna_searchText_mobile','#auto-search-mobile',"");
		}
	$("#dyna_searchText_portable").val("");
	$("#dyna_searchText_mobile").val("");	
	$("#section_inbox").html("");
	$("#section_inbox_mobile").html("");
    $("#showTilesOrNotifications").find(".scroles").remove();
	loadActivities("show",callBackArguments.type);
	
	if(callBackArguments.isFromHistory){
		var temp=getState();
		activity=temp[1];
		setSelectedActivityId(activity);
		if(temp[0]!='#All'){
			
			var tileType=temp[0].substring(1,temp[0].length);
			loadSpecificTiles(tileType);
			}else{
				loadSelectedActivityTiles(activity);
			}
	}
	else{
		if(activitiesData.length>0){
			activity=activitiesData[0].id;
			changeUrl('All',activitiesData[0].id);
		}else{
			 window.location.href='#';
				$(window).unbind('scroll');
				$("#dyna_dashboard").hide();
				$("#dyna_mainContent").show();
			$(".pageheader").html("");
				
				$("#showTilesOrNotifications").html(
				"<div class='scroles scrib_container no_res'> Oops!! Nothing here </div>");
				
		}
	}
	  clearTimeout(callupdates);
		viewUpdates();
	
	
}

/**
 * to show new Updates 
 * @param count
 */

function informNewTiles(count){
	  $("#info_block").hide();
	$(document).prop('title', "ScribLeaf");
	if(count>0){
		  $("#info_block").alert();
	       $("#info_block").fadeTo(10000, 500)
	if(count==1){
	$('#newTiles-alert').html("<span class='scrib-link-over'  onClick=javascript:loadSelectedActivityTiles('"+activity+"')>You Have "+count+" New Update</span>");
	}
	else{
		$('#newTiles-alert').html("<span class='scrib-link-over'  onClick=javascript:loadSelectedActivityTiles('"+activity+"')>You Have "+count+" New Updates</span>");
	}
	$(document).prop('title', "ScribLeaf ("+count+")");
	}
}

/**
 * To hilight the active tiletype
 * @param activityId
 * @param tileType
 */
function makeTileTypeActive(activityId,tileType){
	for(i=0;i<TileTypeArray.length;i++){
		if(TileTypeArray[i]!=tileType){
			$(".active-"+TileTypeArray[i]+"-"+activityId).removeClass("scrib-active");
		}else{
			$(".active-"+tileType+"-"+activityId).addClass("scrib-active");
		}
		
	};
}

/**
 * To heilight the activity
 * @param activityId
 */
function makeActivityActive(activityId){
	$('.scrib_inbox_scrl').scrollTop(1);
	$.each(activitiesData,function(i,activityobject){
		if(activityobject.id==activityId){
			$(".active-"+activityobject.id).addClass('scrib-active');
		}else{
			$(".active-"+activityobject.id).removeClass('scrib-active');
		}
	});
	}
/**
 * To show common Header
 */
function showCommonHeader(){
	var commonTemplate = getTemplate("pageHeader_common");
	var commonHeader = commonTemplate();
	$(".pageheader").html("");
	$(".pageheader").append(commonHeader);
}

/**
 * To loadparticular tiles depends on hashchange
 */
var loadParticularTiles=function(){
	
	var activityFlag=false;
	var temp=getState();
if(temp[0]=="#orgmembers"){
	showActivityMembers(true);
}
else{
	if(temp.length>1){
		
	activity=temp[1];
	 setSelectedActivityId(activity);
	
	$.each(activitiesData,function(i,activityObject){
		if(activityObject.id==temp[1]){
			activityFlag=true;
			
		}
	})
	if(activityFlag){
		$(".count_"+temp[1]).attr("style","display:none");
		if(temp[0]!='#All'){
			var tileType=temp[0].substring(1,temp[0].length);
			loadSpecificTiles(tileType);
			}else{
				loadSelectedActivityTiles(temp[1]);
			}
	}else{
		if(activityType=='internal'){
		loadActivityTypes('external',true);
		
	}else{
		loadActivityTypes('internal',true);
	}
}
}
}
}

/**
 * to get the url
 */
var getState=function(){
	var index = location.href.indexOf("#");
	var text =location.href.substring(index);
	var temp=text.split("-");
	return temp;
}

/**
 * 
 * to change the url
 * @param type
 * @param activityId
 * 
 */
var changeUrl=function(type,activityId){
	 var currentUrl=window.location.href;
	 currentUrl=currentUrl.substring(0,currentUrl.indexOf('#'));
	 window.location.href=currentUrl+"#"+type+"-"+activityId;
	 $(".count_"+activityId).attr("style","display:none");
}



/** code for ie**/


function loadOrgMembers(){
	if(isIe&&accountType=="Enterprise"){
		$(".orgmembers").click(function(){
			window.location.href="#orgmembers";
		});
	}
}